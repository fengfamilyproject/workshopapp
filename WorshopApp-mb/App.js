import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AvariasProvider } from './src/contexts/AvariasContext'
import { JobCardProvider } from './src/contexts/JobCardContext'
import Routes from "./src/routes/index";

const App = () => {
  return (

    <NavigationContainer>
      <JobCardProvider>
      <AvariasProvider>
        <Routes />
      </AvariasProvider>
      </JobCardProvider>     
    </NavigationContainer>
  );
};

export default App;