import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component, useRef, useState, useEffect } from 'react';
import VerJobCard from './VerJobCard'
import JobCardFeed from '../componentes/JobCardFeed'
import { createStore } from 'redux'
import CriarJobCard from '../componentes/CriarJobCard'
import JobCard from '../componentes/JobCard';
import { JobCardProvider, JobCardConsumer } from '../contexts/JobCardContext'

class CriarJobCardPage extends Component {

    render() {
        return (
            <>
                    <ScrollView>
                        <View>
                            <CriarJobCard 
                                navegar = {() => this.props.navigation.navigate('VerJobCards')}
                            />
                        </View>
                    </ScrollView>

            </>
        );
    }

}

export default CriarJobCardPage
