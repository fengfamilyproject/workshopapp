import React, { Component, createRef } from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  Platform,
  KeyboardAvoidingView,
  Text
} from 'react-native';
import NotifyBar from '../componentes/NotifyBar';
import styles from "../styles/index";
import RecuperarSenha from '../componentes/RecuperarSenha'
import LoginHeader from '../componentes/LoginHeader'
import LoginImage from '../componentes/LoginImage'


class Login extends Component {

  clicou = () => {
    Alert.alert("MPDC", "Solicite no Departamento de manutencao")
  }
  autenticar = () => {
   
      this.props.navigation.navigate('Home')
 
  }

  constructor (props){
    super(props);
    this.inputRef = createRef()
  }

  componentDidMount() {
    this.inputRef.current.focus()
    console.log(this.inputRef)
  }


  state = { userName: '', password: '' };

  render() {


    return (
      <>
        <NotifyBar />
        <LoginHeader />

        <View style={styles.container1}>

          <LoginImage />

          <Text style={styles.logText}>Login</Text>

          <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
          >
            <TextInput
              ref={this.inputRef}
              autoCorrect={false}
              returnKeyType={'next'}
              autoCapitalize='none'
              placeholder="Nome do usuário"
              style={styles.inputLogin}
              onChangeText={text => this.setState({ userName: text })}
              value={this.state.userName}
              onSubmitEditing={event => {
                this.input2.focus();
              }}
            />
            <TextInput
              autoCorrect={false}
              placeholder="Senha"
              secureTextEntry={true}
              style={styles.inputLogin}
              onChangeText={text => this.setState({ password: text })}
              value={this.state.password}
              ref={ref => {
                this.input2 = ref;
              }}
            />

            <TouchableOpacity
              style={styles.botaoEnt}
              onPress={this.autenticar}
            >
              <Text style={styles.botaoEntText}>Entrar</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.botaoEsq}
              onPress={RecuperarSenha}
            >
              <Text style={styles.botaoEsqText}>Esqueceu a senha?</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </View>
      </>
    )
  }
}


export default Login
