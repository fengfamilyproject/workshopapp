import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component, useRef, useState, useEffect } from 'react';
import VerJobCard from './VerJobCard'
import JobCardFeed from '../componentes/JobCardFeed'
import { createStore } from 'redux'
import CriarJobCard from '../componentes/CriarJobCard'
import JobCard from '../componentes/JobCard';
import { JobCardProvider, JobCardConsumer } from '../contexts/JobCardContext'
import * as Buttons from '../componentes/Buttons'

class VerJobCards extends Component {

    componentDidUpdate(_, prevState) {
        const { jobCards } = this.state;

    }

    addJobCard = event => {
        event.preventDefault();
        alert('Adicionar JobCard')

        const newJobCard = {
            turno: this.state.turno
        }

        this.setState({
            jobCards: [...this.jobCards, newJobCard],
            newJobCard: '',
        });
        Keyboard.dismiss();
    }

    render() {
        return (
            <>

                <ScrollView>
                    <View style={styles.cardContainer}>

                        <View style={styles.loginHeader}>
                            <Buttons.PrimaryButton
                                title='Novo JobCard'
                                onPress={() => this.props.navigation.navigate('CriarJobCardPage')}
                            />
                        </View>

                        <View>
                            <JobCardFeed />
                        </View>

                    </View>
                </ScrollView>
            </>
        );
    }

}

export default VerJobCards
