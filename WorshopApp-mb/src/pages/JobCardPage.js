import {
  SafeAreaView,
  StyleSheet,
  View,
  Button,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  Text
} from 'react-native';
import NotifyBar from '../componentes/NotifyBar';
import styles from "../styles/index";
import React, { Component } from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


export default class JobCardPage extends Component {

  render() {
    return (

      <>
        <NotifyBar />
        <View style={styles.container2}>
          <View style={styles.container3}>
            <TouchableOpacity
              style={styles.menuBox}
              onPress={() => { this.props.navigation.navigate('CriarJobCardPage') }}
            >
              <MaterialIcons name="note-add" size={75} />
              <Text style={styles.info}>Criar JobCard</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.menuBox}
              onPress={() => { this.props.navigation.navigate('VerJobCards') }}
            >
              <Image
                source={require("../images/assets/jobcard.png")}
                style={styles.icon}
              />
              <Text style={styles.info}>Visualizar JobCards</Text>
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  }
}
