import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component, useRef, useState, useEffect } from 'react';
import AvariasCreate from '../componentes/AvariaCreate'

class AvariasCreatePage extends Component {

    render() {
        return (
            <>
                    <ScrollView>
                        <View>
                            <AvariasCreate 
                                navegar = {() => this.props.navigation.navigate('AvariasPage')}
                            />
                        </View>
                    </ScrollView>
            </>
        );
    }

}

export default AvariasCreatePage
