import {
    SafeAreaView,
    StyleSheet,
    View,
} from 'react-native';
import NotifyBar from '../componentes/NotifyBar';
import styles from "../styles/index";
import React, { Component, useState } from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import IconMaterial from '../componentes/IconMaterial'
import IconImage from '../componentes/IconImage'
import IconNotify from '../componentes/IconNotify'
import sair from '../services/sair'

export default class Home extends Component {

    render() {
        return (

            <>
                <NotifyBar />
                <View style={styles.container2}>
                    <View style={styles.container3}>

                        <IconMaterial page={() => this.props.navigation.navigate('Manutencao')}
                            title='Feedpage'
                            icon='home' />

                        <IconImage page={() => this.props.navigation.navigate('Manutencao')}
                            title='Manutenção'
                            image={require("../images/assets/handyman.png")} />

                        <IconMaterial page={() => this.props.navigation.navigate('Manutencao')}
                            title='Performance'
                            icon='assessment' />

                        <IconMaterial page={() => this.props.navigation.navigate('Manutencao')}
                            title='Alertas'
                            icon='notifications' />


                        <IconMaterial page={() => this.props.navigation.navigate('Manutencao')}
                            title='Conta'
                            icon='person' />

                        <IconMaterial page={() => this.props.navigation.navigate('Manutencao')}
                            title='Procurar'
                            icon='search' />


                        <IconMaterial page={sair}
                            title='Sair'
                            icon='live-help' />
                            
                        <IconNotify 
                            notify='+3'
                            page={() => this.props.navigation.navigate('Manutencao')}
                            title='Manutenção'
                            image={require("../images/assets/handyman.png")} />

                    </View>
                </View>
            </>
        )
    }
}
