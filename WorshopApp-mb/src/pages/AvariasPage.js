import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component } from 'react';
import { AvariasProvider } from '../contexts/AvariasContext'
import * as Buttons from '../componentes/Buttons'
import AvariasFeed from '../componentes/AvariasFeed';

class AvariasPage extends Component {


    render() {
        return (
            <>
                    <ScrollView>
                        <View style={styles.cardContainer}>

                            <View style={styles.loginHeader}>
                                <Buttons.PrimaryButton
                                    title='Nova Avaria'
                                    onPress={() => this.props.navigation.navigate('AvariasCreatePage')}
                                />
                            </View>

                            <View>
                                <AvariasFeed />
                            </View>

                        </View>
                    </ScrollView>

            </>
        );
    }

}

export default AvariasPage
