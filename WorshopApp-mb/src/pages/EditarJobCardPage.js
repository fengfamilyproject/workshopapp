import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component, useRef, useState, useEffect } from 'react';
import EditarJobCard from '../componentes/EditarJobCard'

class EditarJobCardPage extends Component {

    render() {
        return (
            <>
                    <ScrollView>
                        <View>
                            <EditarJobCard 
                                navegar = {() => this.props.navigation.navigate('VerJobCards')}
                            />
                        </View>
                    </ScrollView>

            </>
        );
    }

}

export default EditarJobCardPage