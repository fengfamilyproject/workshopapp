import {
    SafeAreaView,
    StyleSheet,
    View,
    TextInput,
    Image,
    TouchableOpacity,
    Alert,
    Text,
    ScrollView
  } from 'react-native';
import NotifyBar from '../componentes/NotifyBar';
import styles from "../styles/index";
import React, { Component } from 'react';

import {
    ActionsContainer,
    Button,
    FieldsContainer,
    Fieldset,
    Form,
    FormGroup,
    Input,
    Label,
    Switch
  } from 'react-native-clean-form'

  const Ferramentas = () => (
    <ScrollView>
    <Form>
      <FieldsContainer>
        <Fieldset label="Turno:">
          <FormGroup>
            <Label>Manhã/Tarde</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
        <Fieldset label="Data:">
          <FormGroup>
            <Label>DD/MM/AAAA</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
        <Fieldset label="Folha da Obra N°:">
          <FormGroup>
            <Label>XXXX</Label>
            <Input placeholder="Esben" />
          </FormGroup>
          <Fieldset label="Requisitado por:">
          <FormGroup>
            <Label>Nome</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
        <Fieldset label="Equipamento">
          <FormGroup>
            <Label>XXXX</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
        <Fieldset label="Número de Máquina">
          <FormGroup>
            <Label>XXXX</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
        </Fieldset>
        <Fieldset label="Parte do Equipamento">
          <FormGroup>
            <Label>Parte avariada</Label>
            <Input placeholder="Esben" />
          </FormGroup>
        </Fieldset>
      </FieldsContainer>
      <ActionsContainer>
        <Button icon="md-checkmark" iconPlacement="right">Guardar</Button>
      </ActionsContainer>
    </Form>
    </ScrollView>
  )


  export default  Ferramentas
