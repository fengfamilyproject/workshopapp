import {
    View,
    Image,
    TouchableOpacity,
    Alert,
    Text
  } from 'react-native';
import NotifyBar from '../componentes/NotifyBar';
import styles from "../styles/index";
import React, { Component } from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconMaterial from '../componentes/IconMaterial'
import IconImage from '../componentes/IconImage'
import { combineReducers } from 'redux';

export default class Manutencao extends Component {

    render() {
        return (

            <>
                <NotifyBar />
                <View style={styles.container2}>
                    <View style={styles.container3}>

                        <IconMaterial page={() => this.props.navigation.navigate('AvariasPage')} 
                          title='Avarias' 
                          icon='cached' />

                        <IconImage page={() => this.props.navigation.navigate('Manutencao')} 
                          title='Ferramentas e Peças' 
                          image={require("../images/assets/engrenagens.png")} />
                        
                        <IconMaterial page={() => this.props.navigation.navigate('JobCardPage')} 
                          title='JobCards' 
                          icon='library-books' />

                        <IconMaterial page={() => this.props.navigation.navigate('JobCardPage')} 
                          title='Histórico' 
                          icon='history' />
  
          </View>
        </View>
        </>
      );
    }
  }
