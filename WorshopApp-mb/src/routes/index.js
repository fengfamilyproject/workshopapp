import {
    TouchableOpacity,
    Image,
    Button,
    Alert,
    Text
  } from 'react-native';
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../pages/Login';
import Home from '../pages/Home';
import Manutencao from '../pages/Manutencao';
import JobCardPage from '../pages/JobCardPage';
import VerJobCards from '../pages/VerJobCards';
import VerJobCard from '../pages/VerJobCard';
import CriarJobCardPage from '../pages/CriarJobCardPage';
import styles from "../styles/index";
import colors from '../styles/colors'
import HeaderRight from '../componentes/HeaderRight'
import AvariasPage from '../pages/AvariasPage'
import EditarJobCardPage from '../pages/EditarJobCardPage'
import AvariasCreatePage from '../pages/AvariasCreatePage'


const Routes = createStackNavigator ({
    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false
        },
    },
    Home: {
        screen: Home,
        navigationOptions: {
            headerLeft: () => null,
            headerTitle: 'Home',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    Manutencao: {
        screen: Manutencao,
        navigationOptions: {
            headerTitle: 'Manutenção',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    JobCardPage: {
        screen: JobCardPage,
        navigationOptions: {
            headerTitle: 'JobCard',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (
                <TouchableOpacity
                style={styles.boxHeader}

                onPress={ () => { Alert.alert('MPDC','Bem-vindo Leonardo Filipe')}}
            >
                <Text style={styles.textUser}>Leonardo Filipe</Text>
            </TouchableOpacity>
              ),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    CriarJobCardPage: {
        screen: CriarJobCardPage,
        navigationOptions: {
            headerTitle: 'Criar JobCard',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    EditarJobCardPage: {
        screen: EditarJobCardPage,
        navigationOptions: {
            headerTitle: 'Editar JobCard',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    VerJobCards: {
        screen: VerJobCards,
        navigationOptions: {
            headerTitle: 'JobCards Disponíveis',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    VerJobCard: {
        screen: VerJobCard,
        navigationOptions: {
            headerTitle: 'JobCards Criado',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
    AvariasPage: {
        screen: AvariasPage,
        navigationOptions: {
            headerTitle: 'Avarias',
            headerStyle: {
                backgroundColor: colors.header
            },
            headerRight: () => (<HeaderRight />),
            headerTintColor: colors.headerText,
            headerTitleStyle: {
                fontWeight: 'bold',
          },
          headerTitleAlign: 'center'
        },
    },
        AvariasCreatePage:{
            screen: AvariasCreatePage,
            navigationOptions: {
                headerTitle: 'Nova Avaria',
                headerStyle: {
                    backgroundColor: colors.header
                },
                headerRight: () => (<HeaderRight />),
                headerTintColor: colors.headerText,
                headerTitleStyle: {
                    fontWeight: 'bold',
              },
              headerTitleAlign: 'center'
            },
        }
        
});




export default createAppContainer(Routes);
