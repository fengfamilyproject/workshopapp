 import React from 'react'
 import {
     Alert
 } from 'react-native'
 
 const sair = () => {
    Alert.alert('Sair?', 'Tem certeza de que deseja sair?',
        [
            {
                text: 'Não',
                style: 'cancel'
            },
            {
                text: 'Sim',
                onPress: () => this.props.navigation.navigate('Login')
            },
        ],
        { cancelable: false }
    )
}

 
export default sair