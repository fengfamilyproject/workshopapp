module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: '1234abcd',
  database: 'workshopapp',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
