module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('planos', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
      },
      intervencao:{
        type: Sequelize.STRING,
        allowNull: true,
      },
      descricao_avaria_pedido:{
        type: Sequelize.STRING,
        allowNull: true,
        primaryKey: true,
        unique: true,
      },
      diferenca:{
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      numMaquina: {
        type: Sequelize.STRING,
         allowNull: true,
         references:{model:'maquinas', key:'num_maquina'},
         onUpdade:'CASCADE',
         onDelete:'SET NULL',
       },

      last_data:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      last_tipo:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      next_tipo:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      next_data:{
        type: Sequelize.DATE,
        allowNull:false,
      },
      actual_horas:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      actual_data:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      next_horas:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      last_horas:{
        type:Sequelize.DATE,
        allowNull: false,
      },

      activo:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      actual_horas:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      actual_data:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      leitura_horas:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      leitura_data:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      utilizacao:{
        type:Sequelize.INTEGER,
        allowNull:false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('planos');
  },
};
