module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('avarias', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
      },
      descricao_AvariaPedido: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
        unique: true,
      },
      dataAvaria:{
        type: Sequelize.DATE,
        allowNull: false,
        primaryKey: true,
        unique: true,
      },
      num_Maquina: {
        type: Sequelize.STRING,
        allowNull: true,
        references:{model:'maquinas', key:'num_maquina'},
        onUpdade:'CASCADE',
        onDelete:'SET NULL',
       },
      tecnico_id: {
       type: Sequelize.INTEGER,
        allowNull: true,
        references:{model:'users', key:'id'},
        onUpdade:'CASCADE',
        onDelete:'SET NULL',
      },
      supervisor_id: {
        type: Sequelize.INTEGER,
         allowNull: true,
         references:{model:'users', key:'id'},
         onUpdade:'CASCADE',
         onDelete:'SET NULL',
       },
       prioridade:{
         type:Sequelize.STRING,
         allowNull:false,
       },
       estagio:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      observacoes:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      solved:{
        type:Sequelize.DATE,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('avarias');
  },
};
