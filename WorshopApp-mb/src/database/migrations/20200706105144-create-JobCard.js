'use strict';

const { formatTokenType } = require("sucrase/dist/parser/tokenizer/types");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('JobCard', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      turno: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      folhaDeObraNum:{
        type:Sequelize.INTEGER,
        allowNull: false,
      },
      requesitado:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      prioridade:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      equipamento:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      HorasDaMaquina:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      num_maquina: {
        type: Sequelize.STRING,
         allowNull: true,
         references:{model:'maquinas', key:'num_maquina'},
         onUpdade:'CASCADE',
         onDelete:'SET NULL',
       },
       avaria_id:{
        type: Sequelize.INTEGER,
        references: {model: 'avarias', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      },

      descricao_avaria:{
        type: Sequelize.STRING,
        references: {model: 'avarias', key: 'descricao_AvariaPedido'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      },
      data_avaria:{
      type: Sequelize.DATE,
      allowNull: true,
      references:{model:'avarias', key:'dataAvaria'},
      onUpdade:'CASCADE',
      onDelete:'SET NULL',
    },

     descricaoPlano:{
      type: Sequelize.STRING,
      references: {model: 'planos', key: 'descricao_avaria_pedido'},
      onUpdate: 'CASCADE',
      onDelete:'SET NULL',
      allowNull:true,
    },
          plano_id:{
        type: Sequelize.INTEGER,
        references: {model: 'planos', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      },
      data:{
        type: Sequelize.DATE,
        allowNull: true,
      },
      parteEquipamento:{
        type: Sequelize.STRING,
        allowNull: false,
      },

      operacaoNum:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      descricaoTarefa:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      tecnicosEnvolvido:{
        type: Sequelize.INTEGER,
        allowNull: true,
        references:{model:'users', key:'id'},
        onUpdade:'CASCADE',
        onDelete:'SET NULL',
      },
      dataInicial:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      dataFim:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      tipoManutencao:{
        type:Sequelize.STRING,
        allowNull: false,
      },
      tipoTarefa:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      item:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      descricaoPeca:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      referencia:{
        type:Sequelize.STRING,
        allowNull: false,
      },
      quantidade:{
        type:Sequelize.INTEGER,
        allowNull:false,
      },
      comentario:{
        type:Sequelize.STRING,
        allowNull: false
      },
      user_id:{
        type: Sequelize.INTEGER,
        references: {model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      },
      provider_id:{
        type: Sequelize.INTEGER,
        references: {model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      }



    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('JobCard');
  },
};
