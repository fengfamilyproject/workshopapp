module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('maquinas', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,

      },
      tipo_equipamento:{
        type: Sequelize.STRING,
        allowNull: false,

      },
      descricao:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      observacao:{
        type: Sequelize.STRING,
        allowNull: true,
      },
      num_maquina:{
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
      },
      ano_fabrico:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      horas_man:{
        type: Sequelize.FLOAT,
        allowNull: false,
      },

      capacidade:{
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      activo:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },

      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('maquinas');
  },
};
