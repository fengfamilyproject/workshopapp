import Sequelize from 'sequelize';

import User from '../app/models/User'; //temos que importar todos os models
import Maquinas from '../app/models/Maquinas';
import Planos from '../app/models/Planos';
import Avarias from '../app/models/Avarias';
//import JobCard from '../app/models/JobCard';

import databaseConfig from '../config/database';


const models = [User,Maquinas,Planos,Avarias]; //todos os models

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
    .map(model => model.init(this.connection))
    .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
