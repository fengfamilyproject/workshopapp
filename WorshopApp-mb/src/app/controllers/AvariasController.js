import Avarias from '../models/Planos';
import * as Yup from 'yup';
import User from '../models/User';
import Maquinas from '../models/Maquinas';

class AvariasController {
  async store(req, res){
    const schema = Yup.object().shape({
      descricao_AvariaPedido: Yup.string().required(),
      dataAvaria:Yup.date().required(),
      solved:Yup.boolean(),
      observacoes:Yup.string(),
      prioridade:Yup.string().required(),
      estagio:Yup.string(),
      num_Maquina:Yup.string().required(),
      tecnico_id:Yup.number().required(),
      supervisor_id:Yup.number().required(),
    });

    if(!(await schema.isValid(req.body))){
      return res.status(400).json({ error: 'Falha na validacao dos dados'});
    }

    const { descricao_AvariaPedido, dataAvaria, solved, observacoes, prioridade,
      estagio, num_Maquina, tecnico_id,supervisor_id} = req.body;

      const checkIsTecnico = await User.findOne({
        where: { id: tecnico_id, provider: true},
      });

      if (!checkIsTecnico){
        return res.status(401).json({error:'O utilizador selecionado nao e tecnico'});
      }

      const checkIsSupervisor = await User.findOne({
        where: { id: supervisor_id, provider: false},
      });

      if (!checkIsSupervisor){
        return res.status(401).json({error:'O utilizador selecionado nao e Supervisor'});
      }

      const checkMaquinaExist=await Maquinas.findOne({
        where:{numMaquina:num_Maquina},
      });

      if(!checkMaquinaExist){
        return res.status(401).json({error:'O numero da maquina nao existe ou esta mal escrito'});
      }

      const avarias = await Avarias.create({
        descricao_AvariaPedido,
        dataAvaria,
        solved,
        observacoes,
        prioridade,
        estagio,
        num_Maquina,
        tecnico_id,
        supervisor_id
      });

    return res.json(avarias);
  }
}

export default new AvariasController();
