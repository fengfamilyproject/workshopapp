import Maquinas from '../models/Maquinas';
import * as Yup from 'yup';

class MaquinasController{
  async store(req, res){
    const schema = Yup.object().shape({
      numMaquina: Yup.string().required(),
      anoFabrico: Yup.number().required().integer(),
      tipoEquipamento: Yup.string().required(),
      descricao:Yup.string().required(),
      activo:Yup.boolean(),
      horasMan:Yup.number(),
      capacidade:Yup.number(),
      observacao:Yup.string(),

    });

    if(!(await schema.isValid(req.body))){
      return res.status(400).json({ error: 'Falha na validacao dos dados'});
    }

    const maquinaExists = await Maquinas.findOne({where: { numMaquina: req.body. numMaquina}});

    if(maquinaExists){
      return res.status(400).json({error: 'Maquina ja esta registada'});
    }

    const {
      id,
      numMaquina,
      anoFabrico,
      tipoEquipamento,
      descricao,
      horasMan,
      capacidade,
      observacao,}= await Maquinas.create(req.body);

    return res.json({
      id,
      numMaquina,
      anoFabrico,
      tipoEquipamento,
      descricao,
      horasMan,
      capacidade,
      observacao,});
  }
}

export default new MaquinasController();
