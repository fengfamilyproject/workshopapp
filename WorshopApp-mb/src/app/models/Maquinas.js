import Sequelize, { Model } from 'sequelize';

class Maquinas extends Model {
  static init (sequelize) {
    super.init(
      {
        tipoEquipamento:Sequelize.STRING,
        descricao:Sequelize.STRING,
        observacao:Sequelize.STRING,
        numMaquina:Sequelize.STRING,
        anoFabrico:Sequelize.INTEGER,
        capacidade:Sequelize.FLOAT,
        activo:Sequelize.BOOLEAN,
       horasMan:Sequelize.FLOAT,
      },
      {
        sequelize,
      }
    );

    return this;
  }

}

export default Maquinas;
