import Sequelize, { Model } from 'sequelize';

class JobCard extends Model {
  static init (sequelize) {
    super.init(
      {
        turno:Sequelize.STRING,
        folhaDeObraNum:Sequelize.INTEGER,
        requesitado:Sequelize.STRING,
        prioridade:Sequelize.STRING,
        equipamento:Sequelize.STRING,
        HorasDaMaquina:Sequelize.INTEGER,
        data:Sequelize.DATE,
        parteEquipamento:Sequelize.STRING,
        operacaoNum:Sequelize.INTEGER,
        descricaoTarefa:Sequelize.STRING,
        tecnicosEnvolvido:Sequelize.INTEGER,
        dataInicial:Sequelize.DATE,
        dataFim:Sequelize.DATE,
        tipoManutencao:Sequelize.STRING,
        tipoTarefa:Sequelize.STRING,
        item:Sequelize.STRING,
        descricaoPeca:Sequelize.STRING,
        referencia:Sequelize.STRING,
        quantidade:Sequelize.INTEGER,
        comentario:Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }
  static associate(models){
    this.belongsTo(models.Maquinas, { foreignKey: 'num_Maquina',});
    this.belongsTo(models.Avarias,{foreignKey:'avaria_id'});
    this.belongsTo(models.Avarias,{foreignKey:'descricao_avaria'});
    this.belongsTo(models.Avarias,{foreignKey:'data_avaria'});
    this.belongsTo(models.Planos,{foreignKey:'descricaoPlano'});
    this.belongsTo(models.Planos,{foreignKey:'plano_id'});
    this.belongsTo(models.User,{foreignKey:'tecnico_id', as: user});
    this.belongsTo(models.User,{foreignKey:'supervisor_id', as: user});
  }

}

export default JobCard;
