import Sequelize, { Model } from 'sequelize';

class Planos extends Model {
  static init (sequelize) {
    super.init(
      {
        intervencao:Sequelize.STRING,
        descricao_avaria_pedido:Sequelize.STRING,
        diferenca:Sequelize.INTEGER,
        last_data:Sequelize.DATE,
        last_tipo:Sequelize.STRING,
        next_tipo:Sequelize.STRING,
        next_data:Sequelize.DATE,
        actual_horas:Sequelize.INTEGER,
        actual_data:Sequelize.DATE,
        next_horas:Sequelize.DATE,
        last_horas:Sequelize.DATE,
        activo:Sequelize.BOOLEAN,
        actual_horas:Sequelize.DATE,
        actual_data:Sequelize.DATE,
        leitura_horas:Sequelize.DATE,
        leitura_data:Sequelize.DATE,
        utilizacao:Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models){
    this.belongsTo(models.Maquinas, { foreignKey: 'numMaquina'});
  }

}

export default Planos;
