import React, { Component } from 'react'
import {
    Keyboard
} from 'react-native'
import jobCards from '../store/jobCards'
import dataActual from '../services/tempo'


const JobCardContext = React.createContext();

class JobCardProvider extends Component {

    ultimoJobCard = () => {

        const ultimo = this.state.jobCardList.length
        return ultimo
    }

    state = {
        jobCardList: [...jobCards],
        newJobCard: '',
        id: 3,
        turno: '',
        data: dataActual,
        folhaDeObraNum: '',
        prioridade: '',
        requisitado: '',
        equipamento: '',
        HorasDaMaquina: '',
        parteEquipamento: '',
        operacaoNum: '',
        descricaoTarefa: '',
        tecnicosEnvolvido: '',
        dataInicio: '',
        horaInicio: '',
        dataFim: '',
        horaFim: '',
        tipoManutencao: '',
        tipoTarefa: '',
        item: '',
        descricaoPeca: '',
        referencia: '',
        quantidade: '',
        comentario: '',
    }

//VALIDACAO DO NOVO JOBCARD

    setTurno = (itemValue) => {
        this.setState({ turno: itemValue })
    }

    setData = (itemValue) => {
        this.setState({ data: itemValue.target.value })
    }

    setFolha = (itemValue) => {
        this.setState({ folhaDeObraNum: itemValue })
    }

    setPrioridade = (itemValue) => {
        this.setState({ prioridade: itemValue })
    }

    setRequisitado = (itemValue) => {
        this.setState({ requisitado: itemValue })
    }

    setEquipamento = (itemValue) => {
        this.setState({ equipamento: itemValue })
    }

    setHorasDaMaquina = (itemValue) => {
        this.setState({ HorasDaMaquina: itemValue })
    }

    setParteEquipamento = (itemValue) => {
        this.setState({ parteEquipamento: itemValue })
    }

    setOperacaoNum = (itemValue) => {
        this.setState({ operacaoNum: itemValue })
    }

    setDescricaoTarefa = (itemValue) => {
        this.setState({ descricaoTarefa: itemValue })
    }

    setTecnicosEnvolvidos = (itemValue) => {
        this.setState({ tecnicosEnvolvido: itemValue })
    }

    setDataInicio = (itemValue) => {
        this.setState({ dataInicio: itemValue.target.value })
    }

    setHoraInicio = (itemValue) => {
        this.setState({ 
            horaInicio: itemValue.target.value
         })
    }

    setDataFim = (itemValue) => {
        this.setState({ dataFim: itemValue.target.value })
    }

    setHoraFim = (itemValue) => {
        this.setState({ horaFim: itemValue.target.value })
    }

    setTipoManutencao = (itemValue) => {
        this.setState({ tipoManutencao: itemValue })
    }

    setTipoTarefa = (itemValue) => {
        this.setState({ tipoTarefa: itemValue })
    }

    setItem = (itemValue) => {
        this.setState({ item: itemValue })
    }

    setDescricaoPeca = (itemValue) => {
        this.setState({ descricaoPeca: itemValue })
    }

    setReferencia = (itemValue) => {
        this.setState({ referencia: itemValue })
    }

    setQuantidade = (itemValue) => {
        this.setState({ quantidade: itemValue })
    }

    setComentario = (itemValue) => {
        this.setState({ comentario: itemValue })
    }

    //FIM DA VALIDACAO

    //SALVAR JOBCARD (NA CONTA)

    constructor(props) {
        super(props);    
        this.addJobCard = this.addJobCard.bind(this);
      }

    addJobCard (e) {
        e.preventDefault();

        const { jobCardList } = this.state;

        const newJobCard = {
            id: this.state.id,
            turno: this.state.turno,
            data: this.state.data,
            folhaDeObraNum: this.state.folhaDeObraNum,
            prioridade: this.state.prioridade,
            requisitado: this.state.requisitado,
            equipamento: this.state.equipamento,
            HorasDaMaquina: this.state.HorasDaMaquina,
            parteEquipamento: this.state.parteEquipamento,
            operacaoNum: this.state.operacaoNum,
            descricaoTarefa: this.state.descricaoTarefa,
            tecnicosEnvolvido: this.state.tecnicosEnvolvido,
            dataInicio: this.state.dataInicio,
            horaInicio: this.state.horaInicio,
            dataFim: this.state.dataFim,
            horaFim: this.state.horaFim,
            tipoManutencao: this.state.tipoManutencao,
            tipoTarefa: this.state.tipoTarefa,
            item: this.state.item,
            descricaoPeca: this.state.descricaoPeca,
            referencia: this.state.referencia,
            quantidade: this.state.quantidade,
            comentario: this.state.comentario,
        }

        alert(`JobCard "${this.state.id}" adicionado com sucesso!`)
        this.setState({
            jobCardList: [...jobCardList, newJobCard],
            id: this.state.jobCardList.length+1,
            newJobCard: '',
        });
        Keyboard.dismiss();
    }

    render() {

        return (
            <JobCardContext.Provider
                value={{
                    ...this.state,
                    setTurno: this.setTurno,
                    setComentario: this.setComentario,
                    setData: this.setData,
                    setDataFim: this.setDataFim,
                    setDataInicio: this.setDataInicio,
                    setDescricaoPeca: this.setDescricaoPeca,
                    setEquipamento: this.setEquipamento,
                    setHoraFim: this.setHoraFim,
                    setHoraInicio: this.setHoraInicio,
                    setTecnicosEnvolvidos: this.setTecnicosEnvolvidos,
                    setOperacaoNum: this.setOperacaoNum,
                    setPrioridade: this.setPrioridade,
                    setReferencia: this.setReferencia,
                    setRequisitado: this.setRequisitado,
                    setTipoTarefa: this.setTipoTarefa,
                    setTipoManutencao: this.setTipoManutencao,
                    setQuantidade: this.setQuantidade,
                    setFolha: this.setFolha,
                    setHorasDaMaquina: this.setHorasDaMaquina,
                    setItem: this.setItem,
                    setDescricaoTarefa: this.setDescricaoTarefa,
                    setParteEquipamento: this.setParteEquipamento,
                    addJobCard: this.addJobCard,
                    ultimoJobCard: this.ultimoJobCard,
                }}
            >
                {this.props.children}
            </JobCardContext.Provider>
        )
    }
}
const JobCardConsumer = JobCardContext.Consumer;


export { JobCardProvider, JobCardConsumer }

