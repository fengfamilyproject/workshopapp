import React, { Component } from 'react'
import {
    Keyboard
} from 'react-native'
import avarias from '../store/avarias'
import dataActual from '../services/tempo'


const AvariasContext = React.createContext();

class AvariasProvider extends Component {

    ultimaAvaria = () => {

        const ultimo = this.state.jobCardList.length
        return ultimo
    }

    state = {
        avariasList: [...avarias],
        newAvaria: '',
        id: 2,
        name: '',
        horaAvaria: '',
        dataAvaria: dataActual,
        equipamento:'',
        parteEquipamento: '',
        HorasDaMaquina: '',
        descricaoAvaria: '',
        prioridade: '',
        observacoes: '',
    }

    //VALIDACAO DO NOVO JOBCARD

    setName = (itemValue) => {
        this.setState({ name: itemValue.target.value })
    }

    setHoraAvaria = (itemValue) => {
        this.setState({ horaAvaria: itemValue })
    }

    setDataAvaria = (itemValue) => {
        this.setState({ dataAvaria: itemValue })
    }

    setEquipamento = (itemValue) => {
        this.setState({ equipamento: itemValue })
    }

    setParteEquipamento = (itemValue) => {
        this.setState({ parteEquipamento: itemValue })
    }

    setHorasDaMaquina = (itemValue) => {
        this.setState({ HorasDaMaquina: itemValue })
    }

    setDescricaoAvaria = (itemValue) => {
        this.setState({ descricaoAvaria: itemValue })
    }

    setPrioridade = (itemValue) => {
        this.setState({ prioridade: itemValue })
    }

    setObservacoes = (itemValue) => {
        this.setState({ observacoes: itemValue })
    }

    //FIM DA VALIDACAO

    //SALVAR JOBCARD (NA CONTA)

    constructor(props) {
        super(props);
        this.addAvaria = this.addAvaria.bind(this);
    }

    addAvaria(e) {
        e.preventDefault();

        const { avariasList } = this.state;

        const newAvaria = {
            id: this.state.id,
            name: this.state.name,
            horaAvaria: this.state.horaAvaria,
            dataAvaria: this.state.dataAvaria,
            equipamento: this.state.equipamento,
            parteEquipamento: this.state.parteEquipamento,
            HorasDaMaquina: this.state.HorasDaMaquina,
            descricaoAvaria: this.state.descricaoAvaria,
            prioridade: this.state.prioridade,
            observacoes: this.state.observacoes,
        }

        alert(`Avaria "${this.state.id}" adicionado com sucesso!`)
        this.setState({
            avariasList: [...avariasList, newAvaria],
            id: this.state.avariasList.length + 1,
            newAvaria: '',
        });
        Keyboard.dismiss();
    }



    render() {

        return (
            <AvariasContext.Provider
                value={{
                    ...this.state,
                    setName: this.setName,
                    setHoraAvaria: this.setHoraAvaria,
                    setEquipamento: this.setEquipamento,
                    setParteEquipamento: this.setParteEquipamento,
                    setHorasDaMaquina: this.setHorasDaMaquina,
                    setDescricaoAvaria: this.setDescricaoAvaria,
                    setPrioridade: this.setPrioridade,
                    setObservacoes: this.setObservacoes,
                    addAvaria: this.addAvaria,
                    ultimaAvaria: this.ultimaAvaria,
                }}
            >
                {this.props.children}
            </AvariasContext.Provider>
        )
    }
}
const AvariasConsumer = AvariasContext.Consumer;


export { AvariasProvider, AvariasConsumer }
