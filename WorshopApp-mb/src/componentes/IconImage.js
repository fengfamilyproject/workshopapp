import React, {Component} from 'react'
import styles from "../styles/index";
import {
    TouchableOpacity,
    Text,
    Image
} from 'react-native'

export default class IconImage extends Component {

    render() {
        return(
            <TouchableOpacity
                style={styles.menuBox}
                onPress={ this.props.page }
            >
            <Image
                            source={this.props.image}
                            style={styles.icon}
                        />
                <Text style={styles.info}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}
