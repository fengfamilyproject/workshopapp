import React from 'react'
import Login from '../auth/Login'

function Sair() {
    r = confirm('Tem certeza que deseja sair?');
    if (r == true){
        return this.props.navigation.navigate('Login')
    }
}

export default Sair
