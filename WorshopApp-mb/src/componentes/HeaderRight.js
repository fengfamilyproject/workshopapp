import React, {Component} from 'react'
import styles from "../styles/index";
import {
    TouchableOpacity,
    Alert,
    Text
} from 'react-native'

export default class HeaderRight extends Component {

    render() {
        return(
            <TouchableOpacity
            style={styles.boxHeader}

            onPress={ () => { Alert.alert('MPDC','Bem-vindo Leonardo Filipe')}}
        >
            <Text style={styles.textUser}>Leonardo Filipe</Text>
        </TouchableOpacity>
        )
    }
}