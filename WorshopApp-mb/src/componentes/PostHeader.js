import React, {Component} from 'react'
import {
    View,
    Image,
    Text
} from 'react-native'
import styles from '../styles/index'


export default class PostHeader extends Component {

    render(){
        return(
            <View style={styles.card1}>         
                <Image
                    source={this.props.image}
                    style={this.props.style}
                />
                <Text style={styles.textTitulo}>{this.props.title} </Text>  
                <Text style={styles.textCodigo}>ID:{this.props.id}</Text>
            </View>
        )
    }
}

