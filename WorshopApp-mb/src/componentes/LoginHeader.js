import React from 'react'
import {
    View,
    Image,
    Text
} from 'react-native'
import styles from '../styles/index' 

 const LoginHeader = () => {

    return(
        <View style={styles.loginHeader}>
            <Image
                source={require("../images/MPDC.jpg")}
                style={styles.logo}
                />
            <Text style={styles.workText}>WorkshopApp</Text>
            </View>
    )
}

export default LoginHeader