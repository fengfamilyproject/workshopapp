import {
    View,
    TextInput,
    Image,
    TouchableOpacity,
    Alert,
    Button,
    Picker,
    SafeAreaView,
    Text,
    Platform,
    ScrollView,
    Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from "../styles/index";
import React, { Component, useRef } from 'react';
import Tags from "react-native-tags";
import PostHeader from './PostHeader'
import InputNumeric from './form/InputNumeric'
import PostTexts from './PostTexts';
import { AvariasConsumer } from '../contexts/AvariasContext'
import TextField from '@material-ui/core/TextField';
import dataActual from '../services/tempo'

export default class AvariaCreate extends Component {

    //CALENDARIO, DATA, HORA

    /*async componentDidMount() {
  
      if (!context.isLoggedIn) {
        Alert.alert('MPDC', 'Voce precisa iniciar a sessao para aceder a esta pagina',
          [
            {
              text: 'Ok', 
              onPress: () => this.props.navigation.navigate('Login')
            },
          ],
          { cancelable: false }
        )
      }
    }
  */
    //VALIDACAO

    erro = (e) => {

    };

    //SUBMISSAO


    handleSubmit = async () => {
        this.props.navigation.navigate('AvariaShow')
    };

    render() {

        return (
            <>
                <AvariasConsumer>
                    {
                        context => {
                            return (
                                <View style={styles.cardContainer}>

                                    <PostHeader
                                        title='Maintanence Avaria'
                                        id={context.id}
                                        style={styles.logo}
                                        image={require("../images/MPDC.jpg")} />

                                    <ScrollView>

                                        <View style={styles.card2}>


                                            <View>
                                                <PostTexts title='Avaria' />
                                                <TextInput
                                                    value={context.name}
                                                    onChangeText={itemValue => context.setName(itemValue)}
                                                    placeholder="Introduza a avaria"
                                                    style={styles.inputForm1} />

                                            </View>

                                            <PostTexts title='Data' />

                                            <PostTexts title='Data e Hora de Avaria' />

                                            <View style={styles.inputJobCard3}>
                                                <TextField
                                                    onChange={context.setDataAvaria}
                                                    type="date"
                                                    defaultValue="2017-05-24"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                            </View>

                                            <View style={styles.inputJobCard3}>
                                                <TextField
                                                    type="time"
                                                    defaultValue="07:30"
                                                    onChange={context.setHoraAvaria}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    inputProps={{
                                                        step: 300, // 5 min
                                                    }}
                                                />
                                            </View>
                                            <Text>{context.horaInicio}</Text>
                                            <Text style={styles.textCategoria}>{context.dataAvaria}</Text>

                                            <View>
                                                <PostTexts title='Equipamento' />
                                                <TextInput
                                                    value={context.equipamento}
                                                    onChangeText={itemValue => context.setEquipamento(itemValue)}
                                                    placeholder="Equipamento a fazer manutencao"
                                                    style={styles.inputForm1} />

                                            </View>

                                            <View>
                                                <PostTexts title='Parte do Equipamento' />

                                                <TextInput
                                                    placeholder="parteEquipamento"
                                                    style={styles.inputForm1}
                                                    value={context.parteEquipamento}
                                                    onChangeText={itemValue => context.setParteEquipamento(itemValue)}
                                                />
                                            </View>

                                            <View>
                                                <PostTexts title='Horas da Maquina' />

                                                <View style={styles.inputForm1}>
                                                    <InputNumeric
                                                        onChangeText={itemValue => context.setHorasDaMaquina(itemValue)}
                                                        value={context.HorasDaMaquina}
                                                        error={this.erro.bind(this)}
                                                    />
                                                </View>

                                            </View>

                                            <View>

                                                <PostTexts title='Descricao da Avaria' />
                                                <TextInput
                                                    placeholder="Descricao da Avaria"
                                                    multiline={true}
                                                    style={styles.inputForm1}
                                                    value={context.descricaoAvaria}
                                                    onChangeText={itemValue => context.setDescricaoAvaria(itemValue)} />

                                            </View>

                                            <View>
                                                <PostTexts title='Prioridade' />

                                                <View style={styles.inputAvaria3}>
                                                    <Picker
                                                        selectedValue={context.prioridade}
                                                        style={styles.inputForm1}
                                                        onValueChange={(itemValue) => context.setPrioridade(itemValue)}
                                                    >
                                                        <Picker.Item label='Alta' value='Alta' />
                                                        <Picker.Item label='Media' value='Media' />
                                                        <Picker.Item label='Baixa' value='Baixa' />
                                                    </Picker>
                                                </View>
                                            </View>

                                            <View >
                                                <PostTexts title='Observacoes' />
                                                <View style={styles.inputAvaria3}>
                                                    <TextInput
                                                        placeholder={'Escreva...'}
                                                        multiline={true}
                                                        style={styles.inputForm1}
                                                        value={context.observacoes}
                                                        onChangeText={itemValue => context.setObservacoes(itemValue)} />

                                                </View>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    type={'submit'}
                                                    style={styles.saveButton}
                                                    onSubmit={context.addAvaria}
                                                    onPressOut={context.addAvaria}
                                                    onPress={this.props.navegar}
                                                >
                                                    <Text style={styles.botaoEntText}>Guardar</Text>
                                                </TouchableOpacity>

                                            </View>

                                        </View>
                                    </ScrollView>

                                </View>
                            )
                        }
                    }
                </AvariasConsumer>
            </>
        )
    };
}
