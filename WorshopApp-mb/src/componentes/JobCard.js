import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from "../styles/index";
import PostHeader from './PostHeader'
import PostTexts from './PostTexts'
import * as Buttons from './Buttons'

export default class JobCard extends Component {

    render() {
        return (
            <>
                <View style={styles.cardContainer} key={this.props.jobCard.id}>

                    <PostHeader
                        title={'Maintanence JobCard'}
                        id={this.props.jobCard.id}
                        style={styles.image}
                        image={require("../images/MPDC.jpg")}
                    />

                    <View style={styles.card2Linha}>

                        <PostTexts title='Turno' value={this.props.jobCard.turno} />
                        <PostTexts title='Data' value={this.props.jobCard.data} />
                        <PostTexts title='Folha de Obra Num' value={this.props.jobCard.folhaDeObraNum} />
                        <PostTexts title='Prioridade' value={this.props.jobCard.prioridade} />
                        <PostTexts title='Requisitado por' value={this.props.jobCard.requisitado} />
                        <PostTexts title='Equipamento' value={this.props.jobCard.equipamento} />
                        <PostTexts title='Horas Da Maquina' value={this.props.jobCard.HorasDaMaquina} />
                        <PostTexts title='Parte do Equipamento' value={this.props.jobCard.parteEquipamento} />
                        <PostTexts title='Numero da Operacao' value={this.props.jobCard.operacaoNum} />
                        <PostTexts title='Descricao da Tarefa' value={this.props.jobCard.descricaoTarefa} />
                        <PostTexts title='Tecnicos Envolvidos' value={this.props.jobCard.tecnicoEnvolvido} />
                        <PostTexts title='Data de Inicio' value={this.props.jobCard.dataInicio} />
                        <PostTexts title='Hora de Inicio' value={this.props.jobCard.horaInicio} />
                        <PostTexts title='Data da Finalizacao' value={this.props.jobCard.dataFim} />
                        <PostTexts title='Hora da Finalizacao' value={this.props.jobCard.horaFim} />
                        <PostTexts title='Tipo de Manutencao' value={this.props.jobCard.tipoManutencao} />
                        <PostTexts title='Tipo de Tarefa' value={this.props.jobCard.tipoTarefa} />
                        <PostTexts title='Item' value={this.props.jobCard.item} />
                        <PostTexts title='Descricao da Peca' value={this.props.jobCard.descricaoPeca} />
                        <PostTexts title='Referencia' value={this.props.jobCard.referencia} />
                        <PostTexts title='Quantidade' value={this.props.jobCard.quantidade} />
                        <PostTexts title='Comentario' value={this.props.jobCard.comentario} />

                        <View style={styles.container3}>

                            <Buttons.PrimaryButton
                                title='Submeter'
                                onPress={() => alert(`JobCard ${this.props.jobCard.id} submetido com sucesso!`)}
                            />

                            <Buttons.PrimaryButton
                                title='Editar'
                                onPress={() => this.props.navigation.navigate('AvariasPage')}
                            />

                            <Buttons.PrimaryButton
                                title='Remover'
                                onPress={() => alert(`JobCard ${this.props.jobCard.id} removido!`)}
                            />

                        </View>
                    </View>

                </View>
            </>
        )
    }


}
