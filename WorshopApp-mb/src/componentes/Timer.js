import React, { Component } from 'react'
import { Text } from 'react-native'

class Timer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            segundos: 0,
            minutos: 0,
            horas: 0,
            dias: 0,

        };
    }
    tick() {

        this.setState(state => ({
            segundos: state.segundos + 1
        }))

        if (this.state.segundos > 4){
            this.setState (state => ({
                minutos: state.minutos + 1
            }))
        }

        if (this.state.segundos > 4 ) {
            this.setState(state => ({
                segundos: 0
            }))
        } //ZERAR SEGUNDOS

        if (this.state.minutos > 4){
            this.setState (state => ({
                horas: state.horas + 1
            }))
        }

        if ( this.state.minutos > 4 ){
            this.setState(state => ({
                minutos: 0

            }))
        } //ZERAR MINUTOS
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
      }

      componentWillUnmount() {
        clearInterval(this.interval);
      }

    render() {
        return (
            <Text>
                Segundos: {this.state.segundos};
                minutos: {this.state.minutos};
                horas: {this.state.horas}
            </Text>
        )
    }

}

export default Timer
