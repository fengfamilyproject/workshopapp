import {
    View,
    TextInput,
    Image,
    TouchableOpacity,
    Alert,
    Button,
    Picker,
    SafeAreaView,
    Text,
    Platform,
    ScrollView,
    Keyboard,
  } from 'react-native';
  import AsyncStorage from '@react-native-community/async-storage';
  import styles from "../styles/index";
  import React, { Component, useRef } from 'react';
  import Tags from "react-native-tags";
  import PostHeader from './PostHeader'
  import InputNumeric from './form/InputNumeric'
  import PostTexts from './PostTexts';
  import { JobCardConsumer } from '../contexts/JobCardContext'
  import TextField from '@material-ui/core/TextField';
  import dataActual from '../services/tempo'
  
  export default class EditarJobCard extends Component {
  
    erro = (e) => {
  
    };
  
    //SUBMISSAO
  
  
    handleSubmit = async () => {
      this.props.navigation.navigate('VerJobCard')
    };
  
    render() {
  
      return (
        <>
          <JobCardConsumer>
            {
              context => {
                return (
                  <View style={styles.cardContainer}>
  
                    <PostHeader
                      title='Maintanence Jobcard'
                      id={context.JobCardList[1].id}
                      style={styles.logo}
                      image={require("../images/MPDC.jpg")} />
  
                    <ScrollView>
  
                      <View style={styles.card2}>
  
                        <PostTexts title='Turno' />
  
                        <View style={styles.inputJobCard3}>
                          <Picker
                            selectedValue={context.turno}
                            onValueChange={(itemValue) => context.setTurno(itemValue)}
                          >
                            <Picker.Item label='Manha' value='Manha' />
                            <Picker.Item label='Tarde' value='Tarde' />
                            <Picker.Item label='Noite' value='Noite' />
                          </Picker>
                        </View>
  
                        <Text style={styles.textCategoria}>{context.turno}</Text>
  
                        <PostTexts title='Data' />
  
                        <View style={styles.inputJobCard3}>
                          <TextField
                            onChange={context.setData}
                            type="date"
                            defaultValue="2017-05-24"
                            InputLabelProps={{
                              shrink: true,
                            }}
                          />
                         
                        </View>
                        <Text style={styles.textCategoria}>{context.data}</Text>
                        <View>
  
                          <PostTexts title='Folha de Obra Nr' />
  
                          <InputNumeric
                            onChangeText={itemValue => context.setFolha(itemValue)}
                            value={context.folhaDeObraNum}
                            error={this.erro()}
                          />
  
                          <Text>{context.folhaDeObraNum}</Text>
                        </View>
  
                        <View>
                          <PostTexts title='Prioridade' />
  
                          <View style={styles.inputJobCard3}>
                            <Picker
                              selectedValue={context.prioridade}
                              onValueChange={(itemValue) => context.setPrioridade(itemValue)}
                            >
                              <Picker.Item label='Alta' value='Alta' />
                              <Picker.Item label='Media' value='Media' />
                              <Picker.Item label='Baixa' value='Baixa' />
                            </Picker>
                          </View>
  
                        </View>
  
                        <View>
                          <PostTexts title='Requisitado por' />
                          <TextInput
                            value={context.requisitado}
                            onChangeText={itemValue => context.setRequisidado(itemValue)}
                            placeholder="Nome Completo"
                            style={styles.inputJobCard1} />
  
                        </View>
  
                        <View>
                          <PostTexts title='Equipamento' />
                          <TextInput
                            value={context.equipamento}
                            onChangeText={itemValue => context.setEquipamento(itemValue)}
                            placeholder="Equipamento a fazer manutencao"
                            style={styles.inputJobCard1} />
  
                        </View>
  
  
                        <View>
                          <PostTexts title='Horas da Maquina' />
  
                          <InputNumeric
                            onChangeText={itemValue => context.setHorasDaMaquina(itemValue)}
                            value={context.HorasDaMaquina}
                            error={this.erro.bind(this)}
                          />
                        </View>
  
                        <View>
                          <PostTexts title='Parte do Equipamento' />
  
                          <TextInput
                            placeholder="parteEquipamento"
                            style={styles.inputJobCard1}
                            value={context.parteEquipamento}
                            onChangeText={itemValue => context.setParteEquipamento(itemValue)}
                          />
                        </View>
  
  
                        <View>
                          <PostTexts title='Operacao Nr' />
  
                          <InputNumeric
                            onChangeText={itemValue => context.setOperacaoNum(itemValue)}
                            value={context.operacaoNum}
                            error={this.erro()}
                          />
  
                        </View>
  
                        <View>
                          <PostTexts title='Descricao da Tarefa' />
                          <TextInput
                            multiline={true}
                            style={styles.inputJobCard2}
                            value={context.descricaoTarefa}
                            onChangeText={itemValue => context.setDescricaoTarefa(itemValue)}
                          />
                        </View>
  
                        <View>
                          <PostTexts title='Tecnicos Envolvidos' />
                          <View style={styles.inputJobCard1}>
                            <Tags
                              textInputProps={{
                                placeholder: "Adicione o Nome e o Apelido"
                              }}
                              createTagOnString={[';']}
                              initialTags={["dog", "cat", "chicken"]}
                              onChangeTags={tags => context.setTecnicosEnvolvidos(tags)}
                              onTagPress={(index, tagLabel, event, deleted) =>
                                console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                              }
                              inputStyle={{ backgroundColor: "#D0E3E1", borderColor: '#00464A', borderWidth: 2 }}
                              containerStyle={{ justifyContent: "center" }}
                              renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                                <TouchableOpacity
                                  key={`${tag}-${index}`} onPress={onPress}>
  
                                  <Text>{tag[0]}; </Text>
                                </TouchableOpacity>
                              )}
                            />
                          </View>
                          <Text>{context.tecnicosEnvolvido}; </Text>
  
                        </View>
                        <View>
                          <PostTexts title='Data e Hora de Inicio' />
  
                          <View style={styles.inputJobCard3}>
                            <TextField
                              onChange={context.setDataInicio}
                              type="date"
                              defaultValue="2017-05-24"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </View>
  
                          <View style={styles.inputJobCard3}>
                            <TextField
                              type="time"
                              defaultValue="07:30"
                              onChange={context.setHoraInicio}
                              InputLabelProps={{
                                shrink: true,
                              }}
                              inputProps={{
                                step: 300, // 5 min
                              }}
                            />
                          </View>
                          <Text>{context.horaInicio}</Text>
  
                        </View>
  
                        <View>
  
                          <PostTexts title='Data e Hora de Finalizacao' />
  
                          <View style={styles.inputJobCard3}>
                            <TextField
                              onChange={context.setDataFim}
                              type="date"
                              defaultValue="2017-05-24"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </View>
  
                          <View style={styles.inputJobCard3}>
                            <TextField
                              type="time"
                              defaultValue="07:30"
                              onChange={context.setHoraFim}
                              InputLabelProps={{
                                shrink: true,
                              }}
                              inputProps={{
                                step: 300, // 5 min
                              }}
                            />
                          </View>
  
                        </View>
  
                        <View>
  
                          <PostTexts title='Tipo de Manutencao' />
  
                          <View style={styles.inputJobCard3}>
                            <Picker
  
                              selectedValue={context.tipoManutencao}
                              onValueChange={(itemValue, itemIndex) => context.setTipoManutencao(itemValue)}
                            >
                              <Picker.Item label='Planificado' value='manutencaoPlanificada' />
                              <Picker.Item label='Nao Planificado' value='manutencaoNaoPlanificada' />
                            </Picker>
                          </View>
  
                        </View>
  
                        <View>
  
                          <PostTexts title='Tipo de Tarefa' />
  
                          <View style={styles.inputJobCard3}>
                            <Picker
                              selectedValue={context.tipoTarefa}
                              onValueChange={(itemValue, itemIndex) => context.setTipoTarefa(itemValue)}
                            >
                              <Picker.Item label='Avaria' value='Avaria' />
                              <Picker.Item label='Modificacao' value='Modificacao' />
                              <Picker.Item label='Correctiva' value='Correctiva' />
                            </Picker>
                          </View>
  
                        </View>
  
                        <View>
                          <PostTexts title='Item' />
  
                          <TextInput
                            style={styles.inputJobCard1}
                            value={context.item}
                            onChangeText={itemValue => context.setItem(itemValue)} />
  
                        </View>
  
                        <View>
  
                          <PostTexts title='Descricao da Peca' />
                          <TextInput
                            placeholder="descricaoPeca"
                            multiline={true}
                            style={styles.inputJobCard2}
                            value={context.descricaoPeca}
                            onChangeText={itemValue => context.setDescricaoPeca(itemValue)} />
  
                        </View>
  
                        <PostTexts title='Referencia' />
                        <TextInput
                          placeholder="Referencia"
                          style={styles.inputJobCard1}
                          value={context.referencia}
                          onChangeText={itemValue => context.setReferencia(itemValue)} />
  
                        <View>
                          <PostTexts title='Quantidade' />
                          <TextInput
                            placeholder="Quantidade"
                            keyboardType={'numeric'} style={styles.inputJobCard1}
                            value={context.quantidade}
                            onChangeText={itemValue => context.setQuantidade(itemValue)} />
  
                        </View>
  
                        <View>
                          <PostTexts title='Comentario' />
                          <TextInput
                            placeholder={'Escreva...'}
                            multiline={true}
                            style={styles.inputJobCard2}
                            value={context.comentario}
                            onChangeText={itemValue => context.setComentario(itemValue)} />
  
                        </View>
  
                        <View>
                          <TouchableOpacity
                            type={'submit'}
                            style={styles.saveButton}
                            onSubmit={context.addJobCard}
                            onPressOut={context.addJobCard}
                            onPress={this.props.navegar}
                          >
                            <Text style={styles.botaoEntText}>Guardar</Text>
                          </TouchableOpacity>
  
                        </View>
  
                      </View>
                    </ScrollView>
  
                  </View>
                )
              }
            }
          </JobCardConsumer>
        </>
      )
    };
}