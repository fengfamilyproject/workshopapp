import React, {Component} from 'react';
import {
    ProgressBarAndroid,
    StyleSheet,
    Text,
    Button,
    View
 } from 'react-native';
 import styles from "../styles/index";

export default class  Progresso extends Component {

    state= {
        position: 0.0
    }

    incrementar() {

        if (this.state.position >0.9) {
            this.setState({
                position: 0.0
            })
        }

        this.setState (
            (state) => ({
               position: state.position + 0.1
            })
        )
    }

    render() {
        return (
            <>
            <ProgressBarAndroid
                color='788B91'
                indeterminate={false}
                progress={this.state.position}
                />
                <Button
                    onPress = {this.incrementar.bind(this)}
                    title="incrementar"
                />
                </>
        )
    }

}
