import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component } from 'react';
import { createStore } from 'redux'
import JobCard from './JobCard';
import { JobCardProvider, JobCardConsumer } from '../contexts/JobCardContext'

class JobCardFeed extends Component {

    render() {
        return (
            <>
                <View>
                    <JobCardConsumer>
                    {context => {
                          return context.jobCardList.map(jobCard => {
                            return <JobCard key={jobCard.id} jobCard={jobCard} />
                          })                            
                        }}
                    </JobCardConsumer>
                </View>
            </>
        );
    }
}

export default JobCardFeed;