import {
    View,
    AsyncStorage,
    ScrollView
} from 'react-native';
import {
    Text
} from 'react-native'
import styles from "../styles/index";
import React, { Component } from 'react';
import { createStore } from 'redux'
import Avaria from './Avaria';
import { AvariasProvider, AvariasConsumer } from '../contexts/AvariasContext'

class AvariasFeed extends Component {

    render() {
        return (
            <>
                <View>
                    <AvariasConsumer>
                        {context => {
                            return context.avariasList.map(avaria => {
                                return <Avaria key={avaria.id} avaria={avaria} />
                            })
                        }}
                    </AvariasConsumer>
                </View>
            </>
        );
    }
}

export default AvariasFeed;