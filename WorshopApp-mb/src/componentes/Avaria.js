import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from "../styles/index";
import PostHeader from './PostHeader'
import PostTexts from './PostTexts'
import * as Buttons from './Buttons'

export default class Avaria extends Component {

    render() {
        return (
            <>
                <View style={styles.cardContainer} key={this.props.avaria.id}>

                    <PostHeader
                        title={'Maintanence avaria'}
                        id={this.props.avaria.id}
                        style={styles.image}
                        image={require("../images/assets/handyman.png")}
                    />

                    <View style={styles.card2Linha}>
                        
                        <PostTexts title='Avaria' value={this.props.avaria.name} />
                        <PostTexts title='Hora da Avaria' value={this.props.avaria.horaAvaria} />
                        <PostTexts title='Data da Avaria' value={this.props.avaria.dataAvaria} />
                        <PostTexts title='Equipamento' value={this.props.avaria.equipamento} />
                        <PostTexts title='Parte do Equipamento' value={this.props.avaria.parteEquipamento} />
                        <PostTexts title='Horas da Maquina' value={this.props.avaria.HorasDaMaquina} />
                        <PostTexts title='Descricao Avaria' value={this.props.avaria.descricaoAvaria} />
                        <PostTexts title='Prioridade' value={this.props.avaria.prioridade} />
                        <PostTexts title='Observacoes' value={this.props.avaria.observacoes} />

                        <View style={styles.container3}>

                            <Buttons.PrimaryButton
                                title='Submeter'
                                onPress={() => alert(`Avaria ${this.props.avaria.id} submetido com sucesso!`)}
                            />

                            <Buttons.PrimaryButton
                                title='Editar'
                                onPress={() => this.props.navigation.navigate('AvariasPage')}
                            />

                            <Buttons.PrimaryButton
                                title='Remover'
                                onPress={() => alert(`avaria ${this.props.avaria.id} removido!`)}
                            />

                        </View>
                    </View>

                </View>
            </>
        )
    }


}
