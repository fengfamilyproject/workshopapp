import React, { Component } from 'react'
import styles from "../styles/index";
import {
    TouchableOpacity,
    Text,
    View,
    Image
} from 'react-native'

export default class IconNotify extends Component {


    render() {

        const iconOrNo = (props) => {
            if (props =! null) {
                return (
                    <>
                        <View style={styles.contadorContainer}>
                            <View style={styles.contadorIcon}>
                                <Text style={styles.contadorText}>{props.notify}</Text>
                            </View>
                        </View>
                    </>
                )
            }

            else {
                <>
                    <View style={styles.contadorContainer}>
                        <View>
                            <Text style={styles.contadorText}></Text>
                        </View>
                    </View>
                </>
            }
        }

        return (
            <TouchableOpacity
                style={styles.menuBox}
                onPress={this.props.page}
            >
                <View>
                    <View>
                        {iconOrNo(this.props.notify)}
                    </View>

                    <Image
                        source={this.props.image}
                        style={styles.icon}
                    />
                </View>

                <Text style={styles.info}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}