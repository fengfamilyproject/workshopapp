import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import styles from "../../styles/index";
import DatePicker from 'react-native-datepicker';
import TimePicker from 'react-native-simple-time-picker';


function DataHora() {

  return (
    <>
    <Text style={styles.textCategoria}>Data e Hora de Inicio:</Text>

    <View>   
      <DatePicker
        style={styles.inputJobCard3}
        date={data}
        format="DD/MM/YYYY"
        minDate="01/11/2020"
        maxDate="01/11/2025"
        onDateChange={(itemValue) => setDate(itemValue)}
        value={data}
        showTime={true}
      />   
    </View>
    <View style={styles.inputJobCard3}>
        <TimePicker
          selectedHours={selectedHours}
          selectedMinutes={selectedMinutes}
          onChange={(hours) => setHours(hours)}
          onChange={(minutes) => setHours(minutes)}
          />
    </View>
    </>
  )
 
  }