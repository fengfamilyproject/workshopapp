import React from 'react'
import { Text, TextInput, View } from "react-native";
import styles from '../../styles'

const InputNumeric = (props) => {

    const { onChangeText, error, onValueChange, value } = props

    return (
        <View>
        <TextInput
            keyboardType={'numeric'}
            onSubmitEditing={() => {  }}
            value={value}
            onChangeText={onChangeText}
            onValueChange={onValueChange}
            placeholder={'Nr'}
            style={styles.inputJobCard1}
        />
        <Text style={styles.textError}>{error}</Text>
    </View>
    )
    
}

export default InputNumeric