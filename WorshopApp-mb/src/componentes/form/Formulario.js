import {
    View,
    TextInput,
    Image,
    TouchableOpacity,
    Alert,
    Button,
    Picker,
    SafeAreaView,
    Text,
    Platform,
    ScrollView,
    Keyboard,
    AsyncStorage
  } from 'react-native';
import { Form } from '@unform/mobile';
import styles from "../../styles/index";
import React, { Component, useRef } from 'react';
import DatePicker from 'react-native-datepicker';
import TimePicker from 'react-native-simple-time-picker';
import Tags from "react-native-tags";


class Formulario extends Component {

    constructor() {
        super();
        this.formRef = React.createRef()
        this.state = {
          initialData:{ },
          data:'',
          folhaDeObraNum: '',
          dataInicio:'',
          dataFim: '',
          item: '',
          requisitado:'',
          horaInicio:'',
          horaFim:'',
          HorasDaMaquina: '',
          minutoInicio:'',
          descricaoPeca:'',
          descricaoTarefa: '',
          minutoFim:'',
          turno:'',
          prioridade:'',
          tipoManutencao:'',
          tipoTarefa:'',
          tecnicosEnvolvido:'',
          comentario: ''
        }
      }

    

    render(){
        return(
        <>
          <ScrollView>
            <View style={styles.card2}>
              
              <Form 
                onSubmit = {this.handleSubmit}
                ref={this.formRef}
                >

                    <Text style={styles.textCategoria}>Turno:</Text>
                    <View style={styles.inputJobCard3}>
                    <Picker 
                      selectedValue={this.state.turno}
                      onValueChange={(itemValue, itemIndex) => this.setState({turno: itemValue})}
                      >
                      <Picker.Item label='Manha' value='Manha' />
                      <Picker.Item label='Tarde' value='Tarde' />
                      <Picker.Item label='Noite' value='Noite' />
                    </Picker>
                    </View>
                    <Text style={styles.textCategoria}>{this.state.turno}</Text>

                    <Text style={styles.textCategoria}>Data:</Text>
                    <View>   
                      <DatePicker
                        style={styles.inputJobCard3}
                        date={this.state.data}
                        format="DD/MM/YYYY"
                        minDate="01/11/2020"
                        maxDate="01/11/2025"
                        onDateChange={itemValue => this.setState({data:itemValue})}
                        value={this.state.data}
                        showTime={true}
                      />   
                    </View>

                  <Text style={styles.textCategoria}>Folha de Obra Nr:</Text>
                    <TextInput
                      keyboardType={'numeric'}
                      onSubmitEditing={() => {  }}
                      value={this.state.folhaDeObraNum}
                      onChangeText={text => this.validarFolhaNr({text})}
                      placeholder="Nr"
                      style={styles.inputJobCard1}
                      />
                  <Text style={styles.textError}>{this.erro()}</Text>

                  <Text style={styles.textCategoria}>Prioridade:</Text>
                    <View style={styles.inputJobCard3}>
                    <Picker 
                      selectedValue={this.state.prioridade}
                      onValueChange={(itemValue, itemIndex) => this.setState({prioridade:itemValue})}
                      >
                      <Picker.Item label='Alta' value='Alta' />
                      <Picker.Item label='Media' value='Media' />
                      <Picker.Item label='Baixa' value='Baixa' />
                    </Picker>
                    </View>

                    <Text style={styles.textCategoria}>Requisitado por:</Text>
                    <TextInput                       
                      value={this.state.requisitado}
                      onChangeText={itemValue => this.setState({requisitado:itemValue})}
                      placeholder="Nome Completo"
                      style={styles.inputJobCard1} />
                    <Text style={styles.textCategoria}>{this.requisitado}</Text>
                    
                    <Text style={styles.textCategoria}>Equipamento:</Text>
                    <TextInput                       
                      value={this.state.equipamento}
                      onChangeText={itemValue => this.setState({equipamento:itemValue})}
                      placeholder="Nome Completo"
                      style={styles.inputJobCard1} />

                    <Text style={styles.textCategoria}>Horas da Maquina:</Text>
                    <TextInput 
                      keyboardType={'numeric'} 
                      style={styles.inputJobCard1}
                      value={this.state.HorasDaMaquina}
                      onChangeText={itemValue => this.setState({HorasDaMaquina:itemValue})}
                      placeholder="Nr" />

                    <Text style={styles.textCategoria}>Parte do Equipamento:</Text>
                    <TextInput 
                      placeholder="parteEquipamento" 
                        style={styles.inputJobCard1}
                          value={this.state.parteEquipamento}
                            onChangeText={itemValue => this.setState({parteEquipamento:itemValue})}
                    />

                    <Text style={styles.textCategoria}>Operacao N:</Text>
                    <TextInput  
                      placeholder="operacaoNum" 
                      keyboardType={'numeric'}
                      style={styles.inputJobCard1} 
                                               value={this.state.operacaoNum}
                                               onChangeText={itemValue => this.setState({operacaoNum:itemValue})} />

                    <Text style={styles.textCategoria}>Descricao da Tarefa:</Text>
                    <TextInput 
                      name="descricaoTarefa" 
                      multiline={true} 
                      style={styles.inputJobCard2}
                                value={this.state.descricaoTarefa}
                                onChangeText={itemValue => this.setState({descricaoTarefa:itemValue})} />

                    <Text style={styles.textCategoria}>Tecnicos Envolvidos:</Text>
                   
                   <View style={styles.inputJobCard1}>
                    <Tags
                      textInputProps={{
                        placeholder: "Adicione o Nome e o Apelido"
                      }}
                      createTagOnString={[';']}
                      initialTags={["dog", "cat", "chicken"]}
                      onChangeTags={tags => this.setState({tecnicosEnvolvido: tags})}
                      onTagPress={(index, tagLabel, event, deleted) =>
                      console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                      }
                      inputStyle={{ backgroundColor: "#D0E3E1", borderColor: '#00464A', borderWidth:2 }}
                      containerStyle={{ justifyContent: "center" }}
                      renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                      <TouchableOpacity 
                        key={`${tag}-${index}`} onPress={onPress}>

                      <Text>{tag}; </Text>
                      </TouchableOpacity>
                      )}
                    />
                    </View>
                    <Text>{this.state.tecnicosEnvolvido}; </Text>

                    <Text style={styles.textCategoria}>Data e Hora de Inicio:</Text>
                    <View>   
                      <DatePicker
                        style={styles.inputJobCard3}
                        date={this.state.dataInicio}
                        format="DD/MM/YYYY"
                        minDate="01/11/2020"
                        maxDate="01/11/2025"
                        onDateChange={(itemValue) => this.setState({dataInicio: itemValue})}
                        value={this.state.dataInicio}
                        showTime={true}
                      />   
                    </View>
                    <View style={styles.inputJobCard3}>
                        <TimePicker
                          selectedHours={this.state.horaInicio}
                          selectedMinutes={this.state.minutoInicio}
                          onChange={(hours) => this.setState({horaInicio:hours})}
                          onChange={(minutes) => this.setState({minutoInicio: minutes})}
                          />
                    </View>

                    <Text style={styles.textCategoria}>Data e Hora de Finalizacao:</Text>
                    <View>   
                      <DatePicker
                        style={styles.inputJobCard3}
                        date={this.state.dataFim}
                        format="DD/MM/YYYY"
                        minDate="01/11/2020"
                        maxDate="01/11/2025"
                        onDateChange={(itemValue) => this.setState({dataFim: itemValue})}
                        value={this.state.dataFim}
                        showTime={true}
                      />   
                    </View>
                    <View style={styles.inputJobCard3}>
                        <TimePicker
                          selectedHours={this.state.horaFim}
                          selectedMinutes={this.state.minutoFim}
                          onChange={(hours) => this.setState({horaFim: hours})}
                          onChange={(minutes) => this.setState({minutoFim: minutes})}
                          />
                    </View>

                  <Text style={styles.textCategoria}>Tipo de Manutencao:</Text>
                  <View style={styles.inputJobCard3}>
                  <Picker 

                      selectedValue={this.state.tipoManutencao}
                      onValueChange={(itemValue, itemIndex) => this.setState({tipoManutencao: itemValue})}
                      >
                      <Picker.Item label='Planificado' value='manutencaoPlanificada' />
                      <Picker.Item label='Nao Planificado' value='manutencaoNaoPlanificada' />
                    </Picker>
                    </View>

                    <Text style={styles.textCategoria}>Tipo de Tarefa:</Text>
                    <View style={styles.inputJobCard3}>
                    <Picker 
                      selectedValue={this.state.tipoTarefa}
                      onValueChange={(itemValue, itemIndex) => this.setState({tipoTarefa: itemValue})}
                      >
                      <Picker.Item label='Avaria' value='Avaria' />
                      <Picker.Item label='Modificacao' value='Modificacao' />
                      <Picker.Item label='Correctiva' value='Correctiva' />
                    </Picker>
                    </View>

                    <Text style={styles.textCategoria}>Item:</Text>
                    <TextInput 
                      style={styles.inputJobCard1} 
                      value={this.state.item}
                      onChangeText={itemValue => this.setState({item:itemValue})} />

                    <Text style={styles.textCategoria}>Descricao da Peca:</Text>
                    <TextInput 
                      placeholder="descricaoPeca" 
                      multiline={true} 
                      style={styles.inputJobCard2} 
                      value={this.state.descricaoPeca}
                      onChangeText={itemValue => this.setState({descricaoPeca:itemValue})}  />

                    <Text style={styles.textCategoria}>Referencia:</Text>
                    <TextInput 
                      placeholder="referencia" 
                      style={styles.inputJobCard1} 
                      value={this.state.referencia}
                      onChangeText={itemValue => this.setState({referencia:itemValue})} />

                    <Text style={styles.textCategoria}>Quantidade:</Text>
                    <TextInput 
                      placeholder="quantidade" 
                      keyboardType={'numeric'}style={styles.inputJobCard1} 
                      value={this.state.quantidade}
                      onChangeText={itemValue => this.setState({quantidade:itemValue})} />

                    <Text style={styles.textCategoria}>Comentario:</Text>
                    <TextInput 
                      placeholder={'Escreva...'} 
                      multiline={true} 
                      style={styles.inputJobCard2} 
                      value={this.state.comentario}
                      onChangeText={itemValue => this.setState({comentario:itemValue})} />


                  <TouchableOpacity
                    type= {'submit'}
                    style={styles.saveButton}
                    onPress={ () => { this.formRef, this.verificarErros(), this.gravarTotal}}
                  >
                    <Text style={styles.botaoEntText}>Guardar</Text>
                  </TouchableOpacity>
              </Form>
        
              </View>
            </ScrollView>
    </>
        )}
    }

    export default Formulario