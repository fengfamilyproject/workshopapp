import React, {Component} from 'react';
import {
  View,
  Text,

} from 'react-native'
import Picker from 'react-native'
import styles from '../../styles'

class Select3 extends Component {

  render() {

    const {value,label1, label2, label3, value1, value2, value3, selectedValue, onValueChange} = this.props

    return(
      <View style={styles.inputJobCard3}>
      <Picker 
        value={value}
        selectedValue={selectedValue}
        onValueChange={onValueChange}
      >
        <Picker.Item label={label1} value={value1} />
        <Picker.Item label={label2} value={value2} />
        <Picker.Item label={label3} value={value3} />
      </Picker>
    </View>
    )
  }
}

export default Select3