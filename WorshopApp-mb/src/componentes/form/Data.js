import React, { useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import DatePicker from 'react-native-datepicker';
import styles from "../../styles/index";



function Data() {

  const [date, setDate] = useState();

    return (
      <View>   
        <DatePicker
          style={styles.inputJobCard3}
          date={date}
          format="DD/MM/YYYY"
          minDate="01/11/2020"
          maxDate="01/11/2025"
          onDateChange={(itemValue) => setDate(itemValue)}
          value={date}
        />   
        <Text>{date}</Text>
      </View>
      
    )
  }

  export default Data