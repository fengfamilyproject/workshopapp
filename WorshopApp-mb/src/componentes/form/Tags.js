import React from "react";
import { TouchableOpacity, Text } from "react-native";
import Tags from "react-native-tags";

//https://github.com/peterp/react-native-tags

const TagInput = () => (
                           <View style={styles.inputJobCard1}>
                            <Tags
                              initialText="monkey"
                              textInputProps={{
                              placeholder: "Adicione o nome e o Apelido"
                              }}
                              createTagOnString={[';']}
                              initialTags={["dog", "cat", "chicken"]}
                              onChangeTags={tags => console.log(tags)}
                              onTagPress={(index, tagLabel, event, deleted) =>
                              console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                              }
                              containerStyle={{ justifyContent: "center" }}
                              renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                              <TouchableOpacity key={`${tag}-${index}`} onPress={onPress}>
                              <Text>{tag}; </Text>
                              </TouchableOpacity>
                            )}
                            />
                            </View>
)

export default TagInput