import React from 'react'
import { Text, TextInput, View } from "react-native";
import styles from '../../styles'

const InputText = (props) => {

    const { placeholder, onChangeText, error, onValueChange, value } = props

    return (
        <View>
        <TextInput
            value={value}
            onChangeText={onChangeText}
            onValueChange={onValueChange}
            placeholder={placeholder}
            style={styles.inputJobCard1}
        />
        
    </View>
    )
    
}

export default InputText