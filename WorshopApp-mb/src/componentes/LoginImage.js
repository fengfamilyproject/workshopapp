import React from 'react'
import {
    View,
    Text,
    Image
} from 'react-native'
import styles from '../styles/index'

const LoginImage = (props) => {

        return(
            <View>
            <Image
                                source={require("../images/assets/conta-circulo.png")}
                                style={styles.icon}
            />
        </View>
        );    
}

export default LoginImage 