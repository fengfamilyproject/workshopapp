import React, { Component } from 'react'
import styles from "../styles/index";
import {
    TouchableOpacity,
    Text
} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class IconMaterial extends Component {

    render() {
        return (
            <TouchableOpacity
                style={styles.menuBox}
                onPress={this.props.page}
            >
                    <MaterialIcons name={this.props.icon} size={75} />
                    <Text style={styles.info}>{this.props.title}</Text>

            </TouchableOpacity>
        )
    }
}
