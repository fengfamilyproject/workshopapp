import * as React from 'react';
import {StatusBar} from 'react-native'

function NotifyBar() {
    return (
        <StatusBar barStyle="light-content" backgroundColor="#00464A" />
    )
}

export default NotifyBar
