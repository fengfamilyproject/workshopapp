import {
    View,
    Text,
    Platform,
    ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from "../styles/index";
import React, { Component, useRef, useState, useEffect } from 'react';
import store from '../store/index'
import PostHeader from '../componentes/PostHeader'
import JobCard from '../componentes/JobCard'
import { JobCardProvider, JobCardConsumer } from '../contexts/JobCardContext'

export default class NovoJobCard extends Component {

    render() {
        return (
            <View>
                <JobCardConsumer>

                {context => {
                    const ultimo = context.jobCardList.length
                            return (
                            <JobCard key={ultimo} jobCard={context.jobCardList[ultimo]} />
                            )
                        }}

                </JobCardConsumer>
            </View>
        )
    }
}