import React from 'react';
import {
    View,
    Text
} from 'react-native'
import styles from "../styles/index";


const PostTexts = (props) => {

    const { title, value } = props
    return (

        <View style={styles.cardForm1}>
            <Text style={styles.textCategoria}>{title}: </Text>
            <Text1Value value={value} />
        </View>
    )
}

const Text1Value = (props) => {
    const value = props

    return (
        <View style={styles.cardForm2}>
            <Text style={styles.textCategoria2}>{props.value}</Text>
        </View>
    )

}

export default PostTexts