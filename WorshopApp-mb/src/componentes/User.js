import React, {Component} from 'react'
import {
    View,
    Text,
    Image

} from 'react-native'
import Users from './Users'
import styles from '../styles/index'


const User = (props) => {
    return (
        <View style={styles.card2}>
            <Image
                source={require(props.avatarEnd)}
                style={styles.icon} />
                <Text>{props.name}</Text>
        </View>
    )
}