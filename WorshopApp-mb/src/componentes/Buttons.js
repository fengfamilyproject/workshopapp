import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import styles from "../styles/index";

export const PrimaryButton = (props) => {
    return (
        <TouchableOpacity
            type={'submit'}
            style={styles.saveButton}
            onPress={props.onPress}
        >
            <Text style={styles.botaoText}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export const SecondaryButton = (props) => {
    return (
        <TouchableOpacity
            type={'submit'}
            style={styles.saveButton}
            onPress={() => alert('Func. ainda indisponivel!')}
        >
            <Text style={styles.botaoText}>{props.title}</Text>
        </TouchableOpacity>
    )
}

