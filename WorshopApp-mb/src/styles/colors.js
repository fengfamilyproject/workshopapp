
const colors = {
    header: '#788B91',
    headerText: '#CCDEDC',
    backgroundCard: '#B3C7C5',
    backgroundHeader: '#788B91',
    backgroundApp: "#F5F5F5",
    notifyBar: '#00464A',
    inputApp: '#A9DBC6',
    cardText: '#688082'
}

export default colors