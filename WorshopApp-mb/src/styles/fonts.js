
const fonts = {
    input:15, 
    regular:14.5,
    high:19,
    medium:17,
    small:14,
    tiny:13,
}

export default fonts