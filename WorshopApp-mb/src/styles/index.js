import { StyleSheet } from 'react-native'
import colors from './colors'
import fonts from './fonts'
import metrics from './metrics'

const styles = StyleSheet.create({
  loginHeader: {
    backgroundColor: colors.backgroundHeader,
    flexDirection: 'row',
    padding: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:2
  },
  container1: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundCard,
    justifyContent: 'center'
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundApp,
    flexWrap: 'wrap',
    alignContent: 'center',
    padding: metrics.doubleBaseMargin
  },
  container3: {
    flex: 2,
    padding: fonts.high,
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: metrics.smallMargin,
    alignItems: 'center',
  },
  container4: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundApp,
    alignContent: 'center',
    padding: 8
  },
  logo: {
    justifyContent: 'center',
    width: 45,
    height: 45,
    borderRadius: metrics.doubleBaseMargin,
    backgroundColor: colors.backgroundCard,
    margin: metrics.smallMargin,
    marginLeft: metrics.doubleBaseMargin
  },
  image: {
    justifyContent: 'center',
    width: 45,
    height: 45,
    borderRadius: metrics.doubleBaseMargin,
    margin: metrics.smallMargin,
    marginLeft: metrics.doubleBaseMargin
  },
  logText: {
    marginBottom: fonts.high,
    fontSize: 22,
    alignItems: 'center',
    fontWeight: 'bold',
    color: colors.cardText,
  },
  workText: {
    flex: 1,
    fontSize: 22,
    fontWeight: 'bold',
    color: '#CCDEDC',
    marginVertical: fonts.high,
    marginLeft: 32
  },
  contadorText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 10
  },
  contadorIcon: {
    height: 15,
    borderRadius: 5,
    backgroundColor: 'red',
    justifyContent: 'center',
    marginHorizontal:7
  },
  contadorContainer: {
    alignItems: 'flex-end',
    flex:1
  },
  inputLogin: {
    marginTop: metrics.doubleBaseMargin,
    padding: 10,
    width: 300,
    fontSize: fonts.medium,
    backgroundColor: "#FFFFFF",
    borderRadius: 2.5,
    borderWidth: 1,
    borderColor: colors.notifyBar
  },
  botaoEnt: {
    width: 300,
    height: 45,
    backgroundColor: colors.header,
    alignItems: 'center',
    marginVertical: metrics.doubleBaseMargin,
    marginBottom: metrics.quadraBaseMargin,
    borderColor: colors.notifyBar,
    borderWidth: 2,
    borderRadius: 2
  },
  botaoEntText: {
    fontSize: 18,
    color: colors.backgroundApp,
    fontWeight: 'bold',
    padding: 9
  },
  botaoText: {
    fontSize: 18,
    color: colors.backgroundApp,
    padding: 9
  },
  botaoEsq: {
    width: 300,
    height: 42,
    alignItems: 'flex-start',
    marginTop: fonts.high,
  },
  botaoEsqText: {
    fontSize: fonts.high,
    color: colors.cardText,
    padding: 2,
  },
  boxHeader: {
    flexDirection: 'row',
    width: 100,
    height: metrics.doubleBaseMargin,
    alignItems: 'center',
  },
  menuBox: {
    width: 80,
    height: 80,
    alignItems: 'center',
    margin: metrics.baseMargin,
    justifyContent: 'center'
  },
  icon: {
    width: 75,
    height: 75,
  },
  info: {
    fontSize: fonts.tiny,
    color: "#696969",
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center'
  },
  iconHeader: {
    width: 25,
    height: 25,
    marginVertical: fonts.medium
  },
  textUser: {
    marginVertical: metrics.smallMargin,
    flexDirection: 'row',
    fontSize: fonts.tiny,
    color: 'white',
    fontWeight: 'bold'
  },
  abaBar: {
    height: 42,
    backgroundColor: colors.header,
    marginVertical: metrics.doubleBaseMargin,
    marginBottom: 0
  },
  cardContainer: {
    flex:1,
    flexDirection: 'column',
    padding: 2,
    alignItems: 'center',
    justifyContent:'center',
  },
  card1: {
    flexDirection: 'row',
    backgroundColor: colors.header,
    height: 64,
    width: metrics.screenWidth-metrics.quadraBaseMargin,
    paddingLeft: fonts.high,
    marginTop: 9,
    borderTopStartRadius: 3,
    alignContent: 'center'
  },
  card2: {
    alignContent: 'center',
    backgroundColor: colors.backgroundCard,
    marginBottom: 9,
    width: metrics.screenWidth-metrics.quadraBaseMargin,
    padding: 8,
  },
  card2Linha: {
    backgroundColor: colors.backgroundCard,
    marginBottom: 9,
    width: metrics.screenWidth-metrics.quadraBaseMargin,
    padding: 8,
    borderTopStartRadius: 3,
  },
  card3: {
    flex: 2,
    padding: fonts.high,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardForm1: {
    flexDirection: 'row'
  },
  cardForm2: {
    height: 30,
    borderRadius: 5,
    justifyContent: 'center'
  },
  textTitulo: {
    flexDirection: 'row',
    fontSize: fonts.high,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: fonts.medium
  },
  textCodigo: {
    flexDirection: 'row',
    fontSize: fonts.high,
    textAlign: 'center',
    marginVertical: fonts.medium,
    color: colors.notifyBar,
    fontFamily: 'Rubik'
  },
  textCategoria: {
    marginVertical: metrics.smallMargin,
    flexDirection: 'row',
    fontSize: fonts.regular,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'left'
  },
  textCategoria2: {
    flex: 2,
    marginVertical: 4,
    flexDirection: 'row',
    fontSize: fonts.regular,
    color: '#000000',
    textAlign: 'left',
  },
  textPlaceholder: {
    fontSize: 14
  },
  textError: {
    fontSize: fonts.medium,
    color: 'red'
  },
  inputForm1: {
    backgroundColor: colors.inputApp,
    marginVertical: metrics.smallMargin,
    marginLeft: metrics.smallMargin,
    padding: 5,
    paddingLeft: 5,
    marginHorizontal: metrics.baseMargin,
    fontSize: fonts.input,
    justifyContent: 'center',
    borderColor: colors.notifyBar,
    borderWidth: 2,
  },
  inputJobCard2: {
    backgroundColor: colors.inputApp,
    marginVertical: metrics.smallMargin,
    marginLeft: metrics.smallMargin,
    padding: 5,
    marginHorizontal: metrics.baseMargin,
    height: 70,
    fontSize: fonts.input,
    justifyContent: 'center',
    borderColor: colors.notifyBar,
    borderWidth: 2,

  },
  inputJobCard3: {
    height: 44.5,
    width: metrics.screenWidth-(metrics.quadraBaseMargin+ 2*metrics.baseMargin),
    marginLeft: metrics.smallMargin,
    backgroundColor: colors.inputApp,
    marginVertical: metrics.smallMargin,
    padding: 5,
    fontSize: fonts.high,
    justifyContent: 'center',
    borderColor: colors.notifyBar,
    borderWidth: 2,
  },
  saveButton: {
    backgroundColor: colors.header,
    height: 46,
    alignItems: 'center',
    borderRadius: 1,
    borderColor: colors.notifyBar,
    borderWidth: 2,
    marginHorizontal:metrics.baseMargin,
    marginVertical: fonts.high,
    justifyContent: 'center'
  },
  scroll: {

  }
});


export default styles;
