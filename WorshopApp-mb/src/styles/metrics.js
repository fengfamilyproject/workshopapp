
import { Dimensions, Plataform } from 'react-native';

const { width, height } =
    Dimensions.get('window')


const metrics = {
    smallMargin: 5,
    baseMargin: 12,
    doubleBaseMargin: 20,
    quadraBaseMargin: 32,
    screenWidth: width < height ? width :
        height,
        screenHeight: width < height ? height:
        width,
        tabBarHeight: 54,
        navBarHeihgt: 54,
        statusBarHeight:  20,
        baseRadius:3,
};

export default metrics