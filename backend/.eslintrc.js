module.exports = {
    "env": {
       // "es2020": true
       //adicionado
       es6:true,
       node: true,
    },
    "extends": [
        "airbnb-base", "prettier"
    ],

    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
         "prettier"
    ],
    "rules": {
        "prettier/prettier":"error",
        "class-methods-use-this": "off",
        "no-param-reassign": "off",
        "camelcase": "off",
        "no-unused-vars": ["error", { "argsIgnorePattern": "next" }]

    },
    "globals": {
      "Atomics": "readonly",
      "SharedArrayBuffer": "readonly"
    },
};
