import { next } from "sucrase/dist/parser/tokenizer";

import jwt from 'jsonwebtoken';

import { promisify} from 'util';

import authConfig from '../../config/auth';

export default async (req, res , next) => {
  const authHeader =  req.headers.authorization;

  if(!authHeader){
    return res.status(401).json({error: 'Token nao enviado'});
  }

  const [, token] = authHeader.split(' ');

  try {
    const decoded = await promisify(jwt.verify)(token, authConfig.secret);

   req.userId = decoded.id;
   req.planificador = decoded.planificador;
   req.tecnico = decoded.tecnico;
   req.supervisor = decoded.supervisor;

   console.log(decoded);

    return next();

  } catch (err){
    return res.status(401).json({ error: 'Token invalido'});
  }

};
