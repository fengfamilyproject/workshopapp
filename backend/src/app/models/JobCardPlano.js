import Sequelize, { Model } from 'sequelize';

class JobCardPlano extends Model {
  static init(sequelize) {
    super.init(
      {
        turno: Sequelize.STRING,
        folhaDeObraNum: Sequelize.INTEGER,
        requesitado: Sequelize.STRING,
        prioridade: Sequelize.STRING,
        equipamento: Sequelize.STRING,
        HorasDaMaquina: Sequelize.INTEGER,
        data: Sequelize.DATE,
        parteEquipamento: Sequelize.STRING,
        operacaoNum: Sequelize.INTEGER,
        descricaoTarefa: Sequelize.STRING,
        tecnicosEnvolvido: Sequelize.INTEGER,
        dataInicial: Sequelize.DATE,
        dataFim: Sequelize.DATE,
        tipoManutencao: Sequelize.STRING,
        tipoTarefa: Sequelize.STRING,
        item: Sequelize.INTEGER,
        descricaoPeca: Sequelize.STRING,
        referencia: Sequelize.STRING,
        quantidade: Sequelize.INTEGER,
        comentario: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Maquinas, { foreignKey: 'num_Maquina' });
    this.belongsTo(models.Planos, {
      foreignKey: 'descricaoPlano',
      as: 'descricao',
    });
    this.belongsTo(models.Planos, { foreignKey: 'plano_id', as: 'id_plano' });
    this.belongsTo(models.User, { foreignKey: 'tecnico_id', as: 'tecnico' });
    this.belongsTo(models.User, {
      foreignKey: 'supervisor_id',
      as: 'supervisor',
    });
  }
}

export default JobCardPlano;
