import Sequelize, { Model } from 'sequelize';


class Planos extends Model {
  static init (sequelize) {
    super.init(
      {
        intervencao:Sequelize.STRING,
        descricao_avaria_pedido:Sequelize.STRING,
        diferenca:Sequelize.INTEGER,
        last_data:Sequelize.DATE,
        last_tipo:Sequelize.STRING,
        next_tipo:Sequelize.STRING,
        last_horas:Sequelize.DATE,
        activo:Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models){
    this.belongsTo(models.Maquinas, { foreignKey: 'numMaquina'});
  }

}

export default Planos;
