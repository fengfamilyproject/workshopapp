import Sequelize, { Model } from 'sequelize';

class Avarias extends Model {
  static init (sequelize) {
    super.init(
      {
        descricao_avaria_pedido:Sequelize.STRING,
        data_avaria:Sequelize.DATE,
        solved:Sequelize.BOOLEAN,
        observacoes:Sequelize.STRING,
        prioridade:Sequelize.STRING,
        estagio:Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }
  static associate(models){
    this.belongsTo(models.Maquinas, { foreignKey: 'num_maquina'});
    this.belongsTo(models.User,{foreignKey:'tecnico_id', as: 'tecnico'});
    this.belongsTo(models.User,{foreignKey:'supervisor_id', as: 'supervisor'});
  }
}

export default Avarias;
