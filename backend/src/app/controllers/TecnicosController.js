import User from '../models/User';

class ProviderController{
  async index(req, res){
    const tecnicos = await User.findAll({
      where: {tecnico: true},
      attributes: ['id','name'],
    });

    return res.json(tecnicos);
  }
}

export default new ProviderController();
