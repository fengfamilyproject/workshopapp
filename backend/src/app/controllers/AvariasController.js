import Avarias from '../models/Avarias';
import * as Yup from 'yup';
import User from '../models/User';
import Maquinas from '../models/Maquinas';

class AvariasController {
  async store(req, res){
    const schema = Yup.object().shape({
      descricao_avaria_pedido: Yup.string().required(),
      data_avaria:Yup.date().required(),
      solved:Yup.boolean(),
      observacoes:Yup.string(),
      prioridade:Yup.string().required(),
      estagio:Yup.string(),
      num_maquina:Yup.string().required(),
      tecnico_id:Yup.number().required(),
      supervisor_id:Yup.number().required(),
    });

    if(!(await schema.isValid(req.body))){
      return res.status(400).json({ error: 'Falha na validacao dos dados'});
    }

    const { descricao_avaria_pedido, data_avaria, solved, observacoes, prioridade,
      estagio, num_maquina, tecnico_id,supervisor_id} = req.body;

      const checkIsTecnico = await User.findOne({
        where: { id: tecnico_id,tecnico: true},
      });

      if (!checkIsTecnico){
        return res.status(401).json({error:'O utilizador selecionado não é tecnico'});
      }

      const checkIsSupervisor = await User.findOne({
        where: { id: supervisor_id, supervisor: true},
      });

      if (!checkIsSupervisor){
        return res.status(401).json({error:'O utilizador selecionado não é Supervisor'});
      }

      const checkMaquinaExist=await Maquinas.findOne({
        where:{numMaquina:num_maquina},
      });

      if(!checkMaquinaExist){
        return res.status(401).json({error:'O numero da maquina nao existe ou esta mal escrito'});
      }

      const avarias = await Avarias.create({
        descricao_avaria_pedido,
        data_avaria,
        solved,
        observacoes,
        prioridade,
        estagio,
        num_maquina,
        tecnico_id,
        supervisor_id
      });

    return res.json(avarias);
  }
}

export default new AvariasController();
