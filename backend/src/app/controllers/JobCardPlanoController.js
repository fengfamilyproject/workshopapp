import JobCardPlano from '../models/JobCardPlano';
import * as Yup from 'yup';
import Planos from '../models/Planos';
import User from '../models/User';
import Maquinas from '../models/Maquinas';

class JobCardPlanoController{
  async store(req, res){
    const schema = Yup.object().shape({
        turno:Yup.string().required(),
        folhaDeObraNum:Yup.number().required(),
        requesitado:Yup.string().required(),
        prioridade:Yup.string().required(),
        equipamento:Yup.string().required(),
        HorasDaMaquina:Yup.number().required(),
        data:Yup.date().required(),
        parteEquipamento:Yup.string().required(),
        operacaoNum:Yup.number().required(),
        descricaoTarefa:Yup.string().required(),
        tecnicosEnvolvido:Yup.number().required(),
        dataInicial:Yup.date().required(),
        dataFim:Yup.date().required(),
        tipoManutencao:Yup.string().required(),
        tipoTarefa:Yup.string().required(),
        item:Yup.number().required(),
        descricaoPeca:Yup.string().required(),
        referencia:Yup.string().required(),
        quantidade:Yup.number().required(),
        comentario:Yup.string().required(),
    });
    if(!(await schema.isValid(req.body))){
      return res.status(400).json({ error: 'Falha na validacao dos dados'});
    }

    if(!req.supervisor){
      return res.status(401).json({error: 'Nao tem previlegios suficientes'});
    }

    const {
      turno,
      folhaDeObraNum,
      requesitado,
      prioridade,
      equipamento,
      HorasDaMaquina,
      data,
      parteEquipamento,
      operacaoNum,
      descricaoTarefa,
      tecnicosEnvolvido,
      dataInicial,
      dataFim,
      tipoManutencao,
      tipoTarefa,
      item,
      numMaquina,
      descricaoPeca,
      referencia,
      quantidade,
      id_plano,
      comentario,
    }=req.body;

    const checkMaquinaExist=await Maquinas.findOne({
      where:{num_maquina:numMaquina}
    });

    if(!checkMaquinaExist){
      return res.status(401).json({error:'O numero da maquina nao existe ou esta mal escrito'});
    }


    const checkPlanoExist=await Planos.findOne({
      where:{plano_id:id_plano}
    });

    if(!checkPlanoExist){
      return res.status(401).json({error:'O Plano nao existe ou esta mal escrito'});
    }

    const checkTecnicoExist=await User.findOne({
      where:{tecnico_id:id}
    });

    if(!checkTecnicoExist){
      return res.status(401).json({error:'O tecnico indicado nao existe'});
    }

    const checkSupervisorExist=await User.findOne({
      where:{supervisor_id:id}
    });

    if(!checkSupervisorExist){
      return res.status(401).json({error:'O supervisor indicado nao existe'});
    }

    const jobcardplanos= await JobCardPlano.create(req.body);

    return res.json(jobcardplanos);

  }
}

export default new JobCardPlanoController();
