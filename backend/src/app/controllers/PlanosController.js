import Maquinas from '../models/Maquinas';
import Planos from '../models/Planos';
import * as Yup from 'yup';
import User from '../models/User';

class PlanosController {
  async store(req, res){
    const schema = Yup.object().shape({
      intervencao:Yup.string().required(),
      descricao_avaria_pedido:Yup.string().required(),
      activo:Yup.boolean(),

      last_data:Yup.date().required(),
      last_tipo:Yup.string().required(),
      last_horas:Yup.number().required(),

      next_tipo:Yup.string().required(),

      leitura_horas:Yup.number().required(),
      leitura_data:Yup.date().required(),
    });
    if(!(await schema.isValid(req.body))){
      return res.status(400).json({ error: 'Falha na validacao dos dados'});
    }
    const checkIsPlanificador = await User.findOne({
      where: {planificador: true},
    });

    if(!checkIsPlanificador){
      return res.status(401).json({error: 'Nao tem previlegios suficientes'});
    }

    const {
      intervencao,
      descricao_avaria_pedido,
      last_data,
      last_tipo,
      next_tipo,
      numMaquina,
      last_horas,
      activo,
      leitura_horas,
      leitura_data}= req.body;

      const checkMaquinaExist=await Maquinas.findOne({
        where:{num_maquina:numMaquina}
      });

      if(!checkMaquinaExist){
        return res.status(401).json({error:'O numero da maquina nao existe ou esta mal escrito'});
      }

      const planos = await Planos.create(req.body);
      // {
      //   intervencao,
      //   descricao_avaria_pedido,
      //   last_data,
      //   last_tipo,
      //   next_tipo,
      //   numMaquina,
      //   last_horas,
      //   activo,
      //   leitura_horas,
      //   leitura_data
      // }

    return res.json(planos);
  }
}

export default new PlanosController();
