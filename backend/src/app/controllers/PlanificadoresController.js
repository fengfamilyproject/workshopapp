import User from '../models/User';

class PlanificadoresController{
  async index(req, res){
    const planificadores = await User.findAll({
      where: {planificador: true},
      attributes: ['id','name'],
    });

    return res.json(planificadores);
  }
}

export default new PlanificadoresController();
