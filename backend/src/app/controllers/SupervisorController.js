import User from '../models/User';

class SuperVisorController{
  async index(req, res){
    const supervisores = await User.findAll({
      where: {supervisor: true},
      attributes: ['id','name'],
    });

    return res.json(supervisores);
  }
}

export default new SuperVisorController();
