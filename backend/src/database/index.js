import Sequelize from 'sequelize';

import User from '../app/models/User'; //temos que importar todos os models
import Maquinas from '../app/models/Maquinas';
import Planos from '../app/models/Planos';
import Avarias from '../app/models/Avarias';
import JobCardPlano from '../app/models/JobCardPlano';

import databaseConfig from '../config/database';


const models = [User,Maquinas,Planos,Avarias,JobCardPlano]; //todos os models

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
    .map(model => model.init(this.connection))
    .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
