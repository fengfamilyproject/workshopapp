'use strict';

const { formatTokenType } = require("sucrase/dist/parser/tokenizer/types");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('JobCardPlanos', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      turno: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      plano_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references:{model:'planos', key:'id'},
        onUpdade:'CASCADE',
        onDelete:'SET NULL',
       },
      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      folhaDeObraNum:{
        type:Sequelize.INTEGER,
        allowNull: false,
      },
      requesitado:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      prioridade:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      equipamento:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      HorasDaMaquina:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      num_maquina: {
        type: Sequelize.STRING,
         allowNull: true,
         references:{model:'maquinas', key:'num_maquina'},
         onUpdade:'CASCADE',
         onDelete:'SET NULL',
       },

      data:{
        type: Sequelize.DATE,
        allowNull: true,
      },
      parteEquipamento:{
        type: Sequelize.STRING,
        allowNull: false,
      },

      operacaoNum:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      descricaoTarefa:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      tecnicosEnvolvido:{
        type: Sequelize.INTEGER,
        allowNull: true,
        references:{model:'users', key:'id'},
        onUpdade:'CASCADE',
        onDelete:'SET NULL',
      },
      dataInicial:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      dataFim:{
        type:Sequelize.DATE,
        allowNull: false,
      },
      tipoManutencao:{
        type:Sequelize.STRING,
        allowNull: false,
      },
      tipoTarefa:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      item:{
        type:Sequelize.INTEGER,
        allowNull:false,
      },
      descricaoPeca:{
        type:Sequelize.STRING,
        allowNull:false,
      },
      referencia:{
        type:Sequelize.STRING,
        allowNull: false,
      },
      quantidade:{
        type:Sequelize.INTEGER,
        allowNull:false,
      },
      comentario:{
        type:Sequelize.STRING,
        allowNull: false
      },
      tecnico_id:{
        type: Sequelize.INTEGER,
        references: {model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      },

       supervisor_id:{
        type: Sequelize.INTEGER,
        references: {model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete:'SET NULL',
        allowNull:true,
      }
        });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('JobCardPlanos');
  },
};
