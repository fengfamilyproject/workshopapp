module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('planos', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      intervencao:{
        type: Sequelize.STRING,
        allowNull: true,
      },
      descricao_avaria_pedido:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      num_maquina: {
        type: Sequelize.STRING,
         allowNull: false,
         references:{model:'maquinas', key:'num_maquina'},
         onUpdade:'CASCADE',
         onDelete:'SET NULL',
       },

      last_data:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      last_tipo:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      next_tipo:{
        type: Sequelize.STRING,
        allowNull: false,
      },

      last_horas:{
        type:Sequelize.DATE,
        allowNull: false,
      },

      activo:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull:false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('planos');
  },
};
