import { Router } from 'express';

import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import ProviderController from './app/controllers/TecnicosController';
import SuperVisorController from './app/controllers/SupervisorController';
import PlanificadoresController from './app/controllers/PlanificadoresController';

import authMiddleware from './app/middlewares/auth';

import MaquinasController from './app/controllers/MaquinasController';
import AvariasController from './app/controllers/AvariasController';
import PlanosController from './app/controllers/PlanosController';
import JobCardPlanoController from './app/controllers/JobCardPlanoController';

const routes = new Router();

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleware);

routes.put('/users', UserController.update);
routes.get('/tecnicos', ProviderController.index);
routes.get('/supervisores', SuperVisorController.index);
routes.get('/planificadores', PlanificadoresController.index);
routes.post('/maquinas', MaquinasController.store);
routes.post('/planos', PlanosController.store);
routes.post('/avarias', AvariasController.store);
routes.post('/jobcardplanos', JobCardPlanoController.store);
export default routes;
